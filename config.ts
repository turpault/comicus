/* jshint node:true */
import { debug } from "debug";
import { existsSync, mkdirSync, readFileSync } from "fs";
import { argv } from "optimist";
import { tmpdir } from "os";
import { join, basename } from "path";
import { isString } from "underscore";
import * as rimraf from "rimraf";
import { inspect } from "util";

export type Config = {
  port: number;
  scaninterval: number;
  httpsport: number;
  name: string;
  root: string;
  motd: string;
  options: string;
  exclude: string;
  tmp: string;
  cache: string;
  db: string;
  admin_password: string;
  pin_adult: string;
  pin_access: string;
  pin_admin: string;
  auth: boolean;
  clientMode: "bd" | "photo";
  options_debug: boolean;
};

// Find root folder dir
if (!existsSync("package.json")) {
  process.chdir(__dirname);
  while (!existsSync("package.json")) {
    console.info("Going up...");
    process.chdir("..");
  }
}

// Read default configuration file (synchronously)
// Lazy, do it synchronously
const configValue = process.env.config || argv.config || "comicus";
var cfgfile = configValue + ".json";
const configName = basename(configValue);

const cfgPath = cfgfile.startsWith("/") ? cfgfile : join("config", cfgfile);
let cfgDB = { config: {} as Config };
if (existsSync(cfgPath)) {
  cfgDB = JSON.parse(
    readFileSync(cfgPath, {
      encoding: "utf-8",
    })
  );
}

const cfg: Config = cfgDB.config || {};

cfg.port = parseInt(argv.port || process.env.PORT || cfg.port || 8888);
cfg.httpsport = parseInt(argv.httpsport || process.env.HTTPSPORT || cfg.httpsport || 8889);

cfg.name = argv.name || process.env.NAME || cfg.name || "comicus";
cfg.root = argv.root || process.env.ROOT || cfg.root || join(__dirname, "content");

cfg.scaninterval = parseInt(
  argv.scaninterval || process.env.SCANINTERVAL || cfg.scaninterval || 1800
);
cfg.motd = argv.motd || process.env.MOTD || cfg.motd || "";
cfg.options_debug = argv.options_debug || process.env.OPTIONS_DEBUG || cfg.options_debug || false;
cfg.exclude =
  argv.exclude ||
  process.env.EXCLUDE ||
  cfg.exclude ||
  ".DS_Store,.AppleDouble,.TemporaryItems,.apdisk,.stfolder";
cfg.tmp = join(argv.tmp || process.env.COMICUS_TMP || cfg.tmp || tmpdir(), configName);
cfg.cache = join(argv.cache || process.env.COMICUS_CACHE || cfg.cache || tmpdir(), configName);
cfg.db = join(argv.db || process.env.COMICUS_DB || cfg.db || cfg.cache || tmpdir(), configName);
cfg.admin_password =
  argv.admin_password || process.env.COMICUS_ADMIN_PASSWORD || cfg.admin_password || "isa";
cfg.pin_adult = argv.pin_adult || process.env.COMICUS_PIN_ADULT || cfg.pin_adult || "2006";
cfg.pin_admin = argv.pin_admin || process.env.COMICUS_PIN_ADMIN || cfg.pin_admin || "2006";
cfg.pin_access = argv.pin_access || process.env.COMICUS_PIN_ACCESS || cfg.pin_access || "7788";
cfg.clientMode = argv.clientMode || process.env.COMICUS_CLIENT_MODE || cfg.clientMode || "bd";
cfg.auth = cfg.auth === undefined || cfg.auth === true ? true : false;

if (!existsSync(cfg.root)) {
  throw new Error("Content folder could not be found:" + cfg.root);
}
if (!existsSync(cfg.cache)) {
  console.info("Creating cache folder");
  mkdirSync(cfg.cache, { recursive: true });
}
if (!existsSync(cfg.db)) {
  console.info("Creating db folder");
  mkdirSync(cfg.db, { recursive: true });
}
if (existsSync(cfg.tmp)) {
  try {
    rimraf.sync(cfg.tmp);
  } catch (e) {
    console.error(e);
  }
}
console.info("Creating tmp folder");
mkdirSync(cfg.tmp, { recursive: true });

export const debugCfg: boolean = cfg.options_debug;
if (debugCfg && isString(debugCfg)) {
  process.env.DEBUG = debugCfg;
  debug.enable(debugCfg);
}

export const config: Config = cfg;
console.info(configName, ": Current configuration\n", inspect(config));

export const close = async () => {};
