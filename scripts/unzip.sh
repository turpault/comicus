#!/usr/bin/env bash  
#usage: uncompress in_file out_dir
echo "Decompressing $1 to $2"
infile=$1
outdir=$2
mkdir -p "$outdir"
unzip -j -u -o "$infile" -d "$outdir"
"`dirname "$0"`/clearfilenames.sh" "$outdir"
