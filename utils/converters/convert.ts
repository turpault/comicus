/*
    convert.ts
    Copyright (c) Jocelyn Turpault 2021
*/

import d from "debug";
import { join } from "path";
import { Handler, childProcess } from "../worker";
import { config } from "../../config";
import { uuid } from "../fsutils";
import { setInterval2 } from "../timerreport";
import { DecompressedData, Item } from "../types";
const debug = d("comicus:convert");

const pool: Handler[] = [];

export async function convert(
  item: Item,
  format: "pdf" | "epub" | "cbz",
  files: DecompressedData[],
): Promise<string> {
  debug(`Start converting ${item.text}`);
  let worker;
  if (pool.length > 0) {
    worker = pool.shift()!;
  } else {
    const w = childProcess(join(__dirname, "./convert-worker"), () => {
      debug("Child process was killed");
    });
    worker = w;
  }
  const outfile = join(config.tmp, uuid() + "." + format);
  await worker.convert(item, format, files, outfile);
  pool.push(worker);
  return outfile;
}

setInterval2(async () => {
  // Prune the pool queue from time to time
  if (pool.length > 0) {
    debug("Terminating file convert worker");
    const worker = pool.shift()!;
    worker.terminate();
  }
}, 60000);
