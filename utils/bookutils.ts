import { debug as d } from "debug";
import { readFileSync, Stats } from "fs";
import { basename, extname, join } from "path";
import { isString } from "underscore";
import { DecompressedData, Item, StatsWithName } from "./types";
const debug = d("comicus:bookutils");
const IMAGE_EXTENSIONS = ".webp,.jpg,.jpeg,.png,.gif,.jp2".split(",");
const VIDEO_EXTENSIONS = ".webm,.mp4,.mov".split(",");
const FILE_EXTENSIONS = ".cbr,.rar,.cbz,.zip".split(",");
const IMAGE_EXTENSIONS_WITH_EXIF = [".jpg", ".jpeg", "tiff"];

var filepatterns = [
  {
    from: JSON.parse(readFileSync(join("resources", "clean-remove.json"), { encoding: "utf-8" })),
    to: "",
  },
  { from: [".-."], to: "." },
  { from: ["%20"], to: " " },
  { from: [".rar"], to: ".cbr" },
  { from: [".zip"], to: ".cbz" },
  { from: [" .", "_.", "..", "-."], to: "." },
  { from: [/^ /, /^\. /, /^-/], to: "" },
  { from: [/^\./], to: "" },
];

export function isVideo(fName: string): boolean {
  const ext = extname(fName);
  if (ext && VIDEO_EXTENSIONS.includes(ext)) {
    return true;
  }
  return false;
}

export const ignoredExpressions = function () {};
export const sanitizeTitle = function (title: string, removeExtension?: boolean) {
  //debug(title);
  var t = title;
  if (removeExtension) {
    t = t.replace(/\.[^/.]+$/, "");
  }
  var restart = true;
  while (restart) {
    restart = false;
    filepatterns.forEach(function (pattern) {
      var from = pattern.from,
        to = pattern.to;
      for (var i = 0; i < from.length; i++) {
        var r = t.replace(from[i], to);
        if (r != t) {
          t = r;
          restart = true;
        }
      }
    });
  }
  t = t.replace(/\./g, " ");
  if (title != t) {
    //debug('Sanitized ', title, 'to', t);
  }
  try {
    t = decodeURIComponent(t);
  } catch (e) {}
  return t;
};

function extractGroupOfNumbers(a: string): number[] {
  var matched = a.match(/[0-9]+/g);
  if (matched) {
    return matched.map((v) => parseInt(v));
  }
  return [];
}
function compareGroupOfNumbers(g1: number[], g2: number[]): number {
  const m = Math.max(g1.length, g2.length);
  for (let i = 0; i < m; i++) {
    if (g1[i] === g2[i]) {
      continue;
    }
    if (g1[i] === undefined) {
      return 1;
    }
    if (g2[i] === undefined) {
      return -1;
    }
    return g1[i] - g2[i];
  }
  return 0;
}

export const filterBooks = function (p) {
  var ext = extname(p.name || p).toLowerCase();
  if (ext && FILE_EXTENSIONS.includes(ext)) {
    //debug(p, 'is a book',ext);
    return true;
  }
  return false;
};

export function hasExif(p: StatsWithName | string) {
  let ext: string;
  if (isString(p)) {
    ext = extname(p);
  } else {
    ext = extname(p.name);
  }
  return IMAGE_EXTENSIONS_WITH_EXIF.includes(ext.toLowerCase());
}
export const filterImages = function (p: StatsWithName | { name: string } | string) {
  // p can be a stats, a extracted structure or a string
  let filename;
  if (typeof p === "string") {
    filename = p;
  } else if (p instanceof Stats || typeof p.name === "string") {
    filename = p.name;
  }
  if (filename) {
    // Ignore dot files
    if (filename.startsWith(".")) {
      return false;
    }
    var ext = extname(filename).toLowerCase();
    if (ext && IMAGE_EXTENSIONS.includes(ext)) {
      return true;
    }
    if (ext && VIDEO_EXTENSIONS.includes(ext)) {
      return true;
    }
  }
  return false;
};

// Album modification date sorting function
export const sortItemArrayByDateFct = function (a, b) {
  if (a.type == b.type) {
    // Most recent first
    return b.mtime - a.mtime;
  } else {
    if (a.type == "folder") return -1;
    return 1;
  }
};

function dateInText(txt: string): number[] {
  // Get a date from the text
  const toNumbers = (a) => a.map((v) => parseInt(v));
  const tst1 = txt.match(/([0-9]{4}).([0-9]{1,2}).([0-9]{1,2})?/);
  if (tst1) {
    return toNumbers(tst1.slice(1));
  }
  const tst2 = txt.match(/([0-9]{1,2})?.?([0-9]{1,2}).([0-9]{4})/);
  if (tst2) {
    return toNumbers(tst2.slice(1));
  }
  const tst4 = txt.match(/([0-9]{4})/);
  if (tst4) {
    return toNumbers([tst4[0]]);
  }
  return [];
}
export function sortItemByDateInNameFct(a, b) {
  if (a.type == b.type) {
    const da = dateInText(a.text),
      db = dateInText(b.text);
    return compareGroupOfNumbers(da, db);
  }
  if (a.type == "folder") return -1;
  return 1;
}

// Album name sorting function
export function sortItemArrayByNameFct(a: Item, b: Item): number {
  if (a.type === b.type) {
    const withNumbers = compareGroupOfNumbers(
      extractGroupOfNumbers(a.text),
      extractGroupOfNumbers(b.text),
    );
    if (withNumbers !== 0) {
      return withNumbers;
    }
    if (a.text.toLowerCase() < b.text.toLowerCase()) {
      return -1;
    } else if (a.text.toLowerCase() > b.text.toLowerCase()) {
      return 1;
    } else return 0;
  }
  if (a.type == "folder") return -1;

  return 1;
}

export function pageSortAndFilter_1(pages) {
  debug("Starting Page Sort");
  var frontExpressions = ["front", "cover", "couver", "first", "head", "title"];
  var pageWeight = function (page) {
    var base = basename(page.name, extname(page.name)).toLowerCase();
    var weight: number = 1e40;
    frontExpressions.forEach(function (exp) {
      if (base.indexOf(exp) != -1) {
        weight = -10000;
      }
    });
    var numbers = 0;
    var matched = base.match(/[0-9]+/g);
    if (matched) {
      weight = 0;
      matched.forEach(function (match) {
        numbers *= 1000;
        numbers += parseInt(match);
        weight = numbers;
      });
    }

    return weight;
  };
  var sorted = pages.filter(filterImages).sort(function (a, b) {
    var d = pageWeight(a) - pageWeight(b);
    if (d === 0) {
      d = a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
    }
    return d;
  });
  debug("Page sorting - original: ", pages, "sorted", sorted);
  return sorted;
}

// Algo: Group in 3 sections
// 1. pages with more than 2 consecutive numbers
//    a) sort alphabetically
// 2. others
// 3. pages with 'known' markers

export function pageSortAndFilter_2(pages: DecompressedData[]) {
  debug("Starting Page Sort");
  var frontExpressions = ["front", "cover", "couver", "first", "head", "title"];
  const buckets = {
    pages: [] as DecompressedData[],
    others: [] as DecompressedData[],
    ads: [] as DecompressedData[],
  };
  for (const page of pages) {
    if (
      page.name
        .split("/")
        .pop()!
        .match(/[0-9]+/)
    ) {
      // Got a match, seems a page
      buckets.pages.push(page);
    } else if (
      frontExpressions.reduce(
        (prev, current) => prev || page.name.toLowerCase().includes(current),
        false,
      ) === true
    ) {
      buckets.ads.push(page);
    } else {
      buckets.others.push(page);
    }
  }
  // extract page number pattern
  const groups = [] as Array<number>;
  for (const page of buckets.pages) {
    let idx = 0;
    const match = page.name.match(/([0-9]+)/g)!;
    if (match) {
      for (const g of match) {
        groups[idx] = Math.max(groups[idx] || 0, g.length);
        idx++;
      }
    }
  }
  const asGroupString = (pageName: string) => {
    let res: string[] = [];

    const matches = pageName.match(/([0-9]+)/g)!;
    let idx = 0;
    for (const length of groups) {
      const maxStr = "0".repeat(length);
      if (matches[idx]) {
        res.push(maxStr.slice(matches[idx].length) + matches[idx]);
      } else {
        res.push(maxStr);
      }
      idx++;
    }
    return res.join("-");
  };

  buckets.pages = buckets.pages.sort((a, b) => {
    const ga = asGroupString(a.name);
    const gb = asGroupString(b.name);
    if (ga < gb) return -1;
    if (ga > gb) return 1;

    // Fallback on lexicographical is equal
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;

    return 0;
  });

  const sorted = [...buckets.pages, ...buckets.others, ...buckets.ads].filter(filterImages);

  return sorted;
}

export const pageSortAndFilter = pageSortAndFilter_2;
