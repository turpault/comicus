#!/bin/bash  
#usage: uncompress in_file out_dir
echo "Decompressing $1 to $2"
infile=$1
outdir=$2
mkdir -p "$outdir"
type=`file "$infile"`
if [[ "$type" == *RAR* ]]
  then
  unrar e -o- -idp -y "$infile" "$outdir"
  fi

if [[ "$type" == *Zip* ]]
  then
  unzip -j -u -o "$infile" -d "$outdir"
  fi

if [[ "$type" == *JPEG* ]]
  then
  cp "$infile"  "$outdir"
  fi

if [[ "$type" == *GIF* ]]
  then
  cp "$infile"  "$outdir"
  fi

if [[ "$type" == *PNG* ]]
  then
  cp "$infile"  "$outdir"
  fi


cd "$outdir"
for i in *
  do
    mv "$i" "${i//[^0-9A-Za-z_. \-]/}"
  done
