import { ChildProcess, fork } from "child_process";
import { setInterval } from "timers";
import { inspect } from "util";
import Raven from "raven";
import { clearInterval2, setInterval2 } from "./timerreport";

type exposed = (...any) => Promise<any>;

const INACTIVITY_TIMEOUT = 10 * 60 * 1000; // Kill after  x minutes
const EXECUTION_TIMEOUT = 5 * 60 * 1000; // No answer after  x mn, kill
Raven.config(
  "https://aade9d1a4d2e4f0d99ed33a55b5d33f7:975d9b2a203a4e53ac2582bffce26251@sentry.io/162362"
).install();

export function expose(fcts: { [n: string]: exposed }) {
  let lastActivity = new Date().getTime();
  let lastOperation:any = {};
  const originalTitle = process.title;
  setInterval(() => {
    if (lastActivity + INACTIVITY_TIMEOUT < new Date().getTime()) {
      console.warn(
        `Timeout: Terminating process ${originalTitle} / PID = ${
          process.pid
        }. Last operation was ${inspect(lastOperation)}`
      );
      process.exit(0);
    }
  }, 10000);
  setInterval(() => {
    process.send!({
      type: "alive",
    });
  }, 2000);
  process.on("message", (msg: {type:string, id:string, args: any}) => {
    lastActivity = new Date().getTime();
    lastOperation = msg;
    switch (msg.type) {
      case "quit":
        process.exit(0);
      default:
        if (fcts[msg.type]) {
          process.title = "Worker: " + msg.type;
          fcts[msg.type](...(msg.args as Array<any>))
            .then((res) => {
              process.send!({
                type: "result",
                id: msg.id,
                res,
              });
            })
            .catch((error) => {
              try {
                process.send!({
                  type: "error",
                  id: msg.id,
                  error: error.message + "/" + error.stack,
                });
              } catch (e) {
                // Ignore error, parent might be gone
              }
            });
        }
        break;
    }
  });
}

export type Handler = { child: ChildProcess } | any;
export function childProcess(module: string, killed: Function): Handler {
  const requests: { [id: string]: { resolve: Function; reject: Function; [s: string]: any } } = {};
  let id = 0;
  const child = fork(module);
  let keepAlive = new Date().getTime();
  let abort: Function;
  const aliveId = setInterval2(() => {
    if (keepAlive + EXECUTION_TIMEOUT < new Date().getTime()) {
      // No keep alive message, it's probably stuck
      console.info(`Process appears to be stuck while processing ${Object.values(requests).map(r=>r.name).join(' ')}`)
      abort();
    }
  }, 3000);
  abort = () => {
    clearInterval2(aliveId);
    for (const prom of Object.keys(requests)) {
      requests[prom].reject(new Error("terminated"));
      delete requests[prom];
    }

    child.kill("SIGKILL");
    console.warn(`Timeout: Terminating process ${module} / PID = ${child.pid}.`);
    killed();
  };
  child.on("error", (e) => {
    abort();
  });
  child.on("message", (m: any) => {
    if (m.type === "alive") {
      keepAlive = new Date().getTime();
    }
    if (m.type === "result") {
      if (requests[m.id]) {
        requests[m.id].resolve(m.res);
      }
    }
    if (m.type === "error") {
      if (requests[m.id]) {
        requests[m.id].reject(new Error(m.error));
      }
    }
    delete requests[m.id];
  });

  const r = new Proxy(
    {},
    {
      get(target, name): () => Promise<any> {
        return async (...args): Promise<any> => {
          if (name === "terminate") {
            abort();
            return;
          }
          return new Promise((resolve, reject) => {
            id++;
            requests[id] = { resolve, reject, name, args, module };
            child.send({
              type: name,
              id,
              args: args,
            }); // async
          });
        };
      },
    }
  );
  return r;
}
