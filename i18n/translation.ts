import { specialFolders } from "./specialFolders";
import "./string";

var strs: { [key: string]: { [lang: string]: string} } = {
  "Invalid User or Password": { fr: "Usager ou mot de passe invalide" },
  [specialFolders.root]: { en: "Root", fr: "Racine" },
  [specialFolders.favs]: { en: "My Favorites", fr: "Mes Favoris" },
  [specialFolders.recent]: { en: "Added Recently", fr: "Ajouté Récemment" },
  [specialFolders.surprise_file]: { en: "Random Albums", fr: "Albums Au Hasard" },
  [specialFolders.surprise_folder]: { en: "Random Folders", fr: "Séries Au Hasard" },
  [specialFolders.recentfolders]: { en: "Series Updated", fr: "Séries mises à jour" },
  [specialFolders.progressfolders]: { en: "Series In Progress", fr: "Séries en cours" },
  [specialFolders.progress]: { en: "In Progress", fr: "En Cours" },
  [specialFolders.all]: { fr: "Tous", en: "All" },
  [specialFolders.dot]: { en: "Directory", fr: "Répertoires" },
  default: { fr: "Par defaut" },
  "Book not found": { fr: "Livre Inconnu" },
  "Invalid Password": { fr: "Mauvais mot de passe" },
  "FileSystem error with": { fr: "Erreur du fichier" },
  "Invalid User": { fr: "Usager Invalide" },
  "Stop doing replay attacks please": { fr: "La répétition de requete est interdite" },
  Users: { fr: "Utilisateurs" },
  Saved: { fr: "Sauvegardé" },
  Deleted: { fr: "Supprimé" },
  "No such user": { fr: "Utilisateur inconnu" },
  "An error occured": { fr: "Une erreur est survenue" },
  Name: { fr: "Nom d'usager" },
  Kid: { fr: "Enfant" },
  Adult: { fr: "Adulte" },
  "Selected Avatar": { fr: "Avatar Sélectionné" },
  "Available Avatars": { fr: "Avatars Disponibles" },
  Next: { fr: "Suivant" },
  Previous: { fr: "Précédent" },
  Navigation: { fr: "Navigation" },
};

export function replaceAll(s:string , ss: string, r: string): string {
  //var s = this.s.replace(new RegExp(ss, 'g'), r);
  var s = s.split(ss).join(r);
  return s;
};

export function t(s: string, lang: string = "en") {
  lang = (lang || "fr").split("-")[0];
  var r = strs[s] !== undefined ? strs[s][lang] : undefined;
  if (r === undefined && lang == "en") r = s;
  if (r === undefined) {
    r = s;
    console.warn("String not found:", s);
    var stack = new Error().stack;
    console.log(stack);
  }
  return r;
};
