import "mocha";
import { rescanFiles } from "./controllers/book";
import { log, ping } from "./controllers/system";
import { ComicusServer } from "./index";

var server = new ComicusServer();

before(async function () {
  this.timeout(0);
  return server.init();
});

after(async () => {
  await server.close();
  console.log("Server closed, should exit soon");
  console.info(require("./utils/timerreport").listTimers());
});

describe("Comicus tests", function () {
  this.timeout(0);
  it("should respond to ping", ping);

  it("should receive logs", async function () {
    return log({ title: "test" }, "127.0.0.1");
  });

  it("should rescan and generate cache", rescanFiles);

  return;
});
