openssl x509 -in aps.cer -inform DER -outform PEM -out aps.pem
openssl x509 -in aps_development.cer  -inform DER -outform PEM -out aps_development.pem
openssl pkcs12 -in key.p12 -out key.pem -nodes
