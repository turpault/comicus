import { compile, TemplateFunction } from "ejs";
import express from "express";
import { readFileSync } from "fs";
import { join } from "path";
import useragent from "useragent";
import { config } from "../config";
import { folder, itemFromId, pages, search } from "../controllers/book";
import {
  avatars,
  get as getPersona,
  newPersona,
  personaAuth,
  values,
} from "../controllers/persona";
import "../i18n/translation";
import { t } from "../i18n/translation";
import { basicauth, _encodeContentKey } from "../middleware/auth";
import { isVideo } from "../utils/bookutils";
import { Item } from "../utils/types";
import { sanitize } from "../utils/utils";

// file deepcode ignore XSS: <please specify a reason of ignoring this>

const templateNames = [
  "error",
  "test",
  "album-folder",
  "personas",
  "editPersona",
  "album",
  "pages-simple",
  "download",
];

const templates: { [name: string]: TemplateFunction } = {};
for (const name of templateNames) {
  const filename = join("app2", "top-level", name + ".ejs");
  templates[name] = compile(readFileSync(filename, { encoding: "utf-8" }), { filename });
}

function createLink(base: string, req) {
  // forward parameters
  const copyArgs = ["lang", "persona", "density", "sort"];
  for (const a of copyArgs) {
    if (req.query[a]) {
      base += (base.includes("?") ? "&" : "?") + `${a}=${req.query[a]}`;
    }
  }
  return base;
}

function assetLink(base: string, req) {
  // forward parameters
  const args = { contentkey: _encodeContentKey(config.name), ...req.query };
  const copyArgs = ["contentkey"];
  for (const a of copyArgs) {
    if (args[a]) {
      base += (base.includes("?") ? "&" : "?") + `${a}=${args[a]}`;
    }
  }
  return base;
}

type Links = {
  home: string;
  users: string;
  downloadPdf: string;
  downloadCbz: string;
  downloadEpub: string;
  back: string;
  next: string | undefined;
  previous: string | undefined;
  nextName: string | undefined;
  previousName: string | undefined;
};
function getLinks(item, req): Links {
  return {
    home: createLink("/app2?route=folder&id=root", req),
    users: createLink("/app2?route=personas", req),
    downloadPdf: item ? createLink(`/pdf/${item.uid}`, req) : "",
    downloadCbz: item ? createLink(`/cbz/${item.uid}`, req) : "",
    downloadEpub: item ? createLink(`/epub/${item.uid}`, req) : "",
    back: createLink("/app2?route=folder&id=" + (item ? item.parentuid : ""), req),
    next: undefined,
    previous: undefined,
    nextName: undefined,
    previousName: undefined,
  };
}

export async function error(res, text) {
  const html = await templates.error({ text, t });
  res.send(html);
}

const routes = {
  ping: {
    handler: async (req, res) => {
      res.send("<html/>");
    },
  },
  test: {
    handler: async (req, res) => {
      const html = await templates.test({
        title: "foo",
        items: [{ link: "#", text: "foo", thumb: "#", links: [] }],
        config: {},
        links: [],
        t,
      });
      res.send(html);
    },
  },
  folder: {
    auth: true,
    handler: async (req, res) => {
      if (!["bd", "photo"].includes(config.clientMode)) {
        throw new Error("Unknown config type");
      }
      const viewType = {
        bd: "album-folder",
        photo: "album-folder",
      }[config.clientMode];

      const iconSize = {
        bd: 200,
        photo: 200,
      }[config.clientMode];

      const root = {
        bd: "",
        photo: "all",
      }[config.clientMode];

      const sort = {
        bd: "",
        photo: "date-",
      }[config.clientMode];

      const folderid = sanitize(req.query.id) || root;

      // Set default based on clientMode
      req.options.sort = req.options.sort || sort;
      // deepcode ignore HTTPSourceWithUncheckedType: <please specify a reason of ignoring this>
      const searchString = typeof req.query.search === "string" ? req.query.search : "";
      let searchParts: string | undefined | string[] = sanitize(searchString);
      // Empty string = no search
      if (searchParts === "") {
        searchParts = undefined;
      }
      // Empty array = no search
      if (Array.isArray(searchParts) && searchParts.length === 0) {
        searchParts = undefined;
      }
      // single string = search
      if (searchParts && !Array.isArray(searchParts)) {
        searchParts = [searchParts];
      }

      let items: Array<Item & { link?: string; thumb?: string }>;
      if (searchParts) {
        items = await search(searchParts, req.personaid, req.options);
      } else {
        // file deepcode ignore PT: <please specify a reason of ignoring this>
        items = await folder(folderid, req.personaid, req.options);
      }

      // file deepcode ignore PT: <please specify a reason of ignoring this>
      const item = itemFromId(folderid, req.query.lang, req.personaid);
      const title = item ? item.text : folderid ? t(folderid.split("~")[0]) : "";

      for (const folderOrAlbum of items) {
        folderOrAlbum.link = createLink(
          folderOrAlbum.type === "folder"
            ? `/app2?route=folder&id=${folderOrAlbum.uid}`
            : `/app2?route=album&id=${folderOrAlbum.uid}`,
          req,
        );
        folderOrAlbum.thumb = assetLink(
          `/thumbnail/${folderOrAlbum.uid}?width=${iconSize}&height=${iconSize}`,
          req,
        );
        if (config.clientMode === "bd") {
          // Open first page when reading bds
          folderOrAlbum.link += "#nanogallery/imageItems/0/1";
        }
      }

      const html = await templates[viewType]({
        useVersion1: false,
        special: !item,
        uid: item ? item.uid : undefined,
        items,
        title,
        config: { name: config.name, skipPersonaCheck: true, use_ng_1: req.legacyIOS },
        assets: { favicon: config.name, splash: "splash-" + config.name },
        links: getLinks(item, req),
        search: searchParts,
        clientMode: config.clientMode,
      });

      res.send(html);
    },
  },
  personas: {
    handler: async (req, res) => {
      const personaList = await values();

      const items: Item[] = [];
      for (const persona of personaList) {
        items.push(
          Object.assign(
            {
              link: createLink(`/app2?id=root&route=folder&persona=${persona.info.uid}`, req),
              thumb: `/avatar/${persona.info.avatarid}`,
            },
            persona.info,
          ),
        );
      }
      let title = t("Users", req.query.lang);
      const links = {};
      const html = await templates.personas({
        items,
        title,
        config: { name: config.name, skipPersonaCheck: true, use_ng_1: req.legacyIOS },
        assets: { favicon: config.name, splash: "splash-" + config.name },
        links,
        t,
      });

      res.send(html);
    },
  },
  persona: {
    handler: async (req, res) => {
      if (!basicauth(req, res)) {
        return;
      }
      let persona;
      persona = await getPersona(req.query.id);
      if (!persona) {
        persona = newPersona({ uid: req.query.id });
      }
      if (!persona) {
        return error(res, t("No such user", req.query.lang) + ": " + req.query.id);
      }

      const a = await avatars();
      const links = {
        back: "/app2?route=personas&edit=1",
      };

      const html = await templates.editPersona({
        persona: persona.info,
        avatars: a,
        config: { name: config.name, skipPersonaCheck: true, use_ng_1: req.legacyIOS },
        assets: { favicon: config.name, splash: "splash-" + config.name },
        links,
        t,
      });

      res.send(html);
    },
  },
  album: {
    auth: true,
    handler: async (req, res) => {
      const albumid = req.query.id;
      const pageList = await pages(albumid, req.personaid, req.options);
      const resize = req.query.density
        ? `?width=${req.query.density}&height=${req.query.density}`
        : "";

      // Filter and update thumbnail
      const withThumbs = pageList.map((p) => {
        if (isVideo(p.path)) {
          return { ...p, thumb: "/resources/video.png" };
        }
        return { ...p, thumb: p.url + `?width=200&height=200` };
      });
      let items = withThumbs.map((p, idx) => {
        const lat = p?.exif?.gps?.GPSLatitude;
        const long = p?.exif?.gps?.GPSLongitude;
        const video = isVideo(p.path);
        let GPSLocation = "";
        if (lat && long) {
          const fLat =
            (lat[0] + lat[1] / 60 + lat[2] / 3600) * (p.exif.gps.GPSLatitudeRef == "N" ? 1 : -1);
          const fLong =
            (long[0] + long[1] / 60 + long[2] / 3600) *
            (p.exif.gps.GPSLongitudeRef == "W" ? -1 : 1);
          GPSLocation = `${fLat}, ${fLong}`;
        }
        const text = decodeURIComponent(p.url.split("/").pop()!).split(".")[0].substr(0, 8);

        return {
          text,
          link: createLink(
            `/app2?route=pages?albumid=${albumid}&start=${idx}&sort=${req.query.sort}`,
            req,
          ),
          thumb: assetLink(p.thumb, req),
          image: video ? p.url : assetLink(p.url + resize, req),
          id: idx,
          time: p?.exif?.DateTimeOriginal,
          desc: p?.exif?.image?.Model,
          exposure: ((v) => `1/${Math.round(1 / (v ? v : 1))}`)(p?.exif?.exif?.ExposureTime),
          iso: p?.exif?.exif?.ISO,
          GPSLocation,
        };
      });
      const item = itemFromId(req.query.id, req.query.lang, req.personaid);
      const title = item ? item.text : "";
      const links = getLinks(item, req);
      // Add 'next album link
      if (item?.parentuid) {
        const folderData = await folder(item?.parentuid, req.personaid, req.options);
        const idx = folderData.map((item) => item.uid).indexOf(item?.uid);
        if (folderData.length > 1) {
          const nextIdx = (idx + 1) % folderData.length;
          const previousIdx = (idx - 1 + folderData.length) % folderData.length;
          links.next = createLink(`/app2?route=album&id=${folderData[nextIdx].uid}`, req);
          links.nextName = folderData[nextIdx].text;
          links.previous = createLink(`/app2?route=album&id=${folderData[previousIdx].uid}`, req);
          links.previousName = folderData[previousIdx].text;
          if (config.clientMode === "bd") {
            // Open first page when reading bds
            links.next += "#nanogallery/imageItems/0/1";
            links.previous += "#nanogallery/imageItems/0/1";
          }
        }
      }
      const html = await templates.album({
        items,
        title,
        config: { name: config.name, use_ng_1: req.legacyIOS },
        assets: { favicon: config.name, splash: "splash-" + config.name },
        links,
        item,
        open: { id: config.clientMode === "bd" && items.length > 0 ? items[0].id : undefined },
        clientMode: config.clientMode,
        t,
      });

      res.send(html);
    },
  },
  pages: {
    auth: true,
    handler: async (req, res) => {
      const albumid = req.query.id;
      const pageList = await pages(albumid, req.personaid, req.options);
      const resize = req.query.density
        ? `?width=${req.query.density}&height=${req.query.density}`
        : "";

      const items = pageList.map((p) => ({ src: `${p.url}${isVideo(p.path) ? "" : resize}` }));
      const item = itemFromId(albumid, req.query.lang, req.personaid);
      const title = item ? item.text : "";
      const html = await templates['"pages-simple-' + req.query.partial ? "partials" : "top-level"](
        {
          items,
          title,
          config: { name: config.name, skipPersonaCheck: true, use_ng_1: req.legacyIOS },
          assets: { favicon: config.name, splash: "splash-" + config.name },
          links: getLinks(item, req),
        },
      );

      res.send(html);
    },
  },
  download: {
    auth: true,
    handler: async (req, res) => {
      const html = await templates["download"]({
        config: { name: config.name, skipPersonaCheck: true, use_ng_1: req.legacyIOS },
        assets: { favicon: config.name, splash: "splash-" + config.name },
      });

      res.send(html);
    },
  },
  nothing: {
    handler: async (req, res) => {
      error(res, "Nothing here: " + req.originalUrl);
    },
  },
};

export async function install(app) {
  // Compile the source code

  app.use("/app2/resources", express.static(join("app2", "resources")));
  app.use("/resources", express.static(join("resources")));

  app.use("/app2", async (req, _res, next) => {
    const agent = useragent.parse(req.headers["user-agent"]);
    req.legacyIOS = agent.os.family === "iOS" && parseInt(agent.os.major) <= 9;
    next();
  });
  app.use("/app2", async (req, res, next) => {
    if (req.query.auth) {
      personaAuth(req, res, next);
    } else {
      next();
    }
  });

  app.getAsync("/app2", async (req, res) => {
    if (!req.query.route) {
      res.redirect("/app2?route=personas");
    } else {
      const handlerFct = routes[req.query.route].handler;
      if (handlerFct) {
        try {
          await handlerFct(req, res);
        } catch (e) {
          debugger;
          error(res, e.message);
        }
      } else {
        res.redirect("/app2");
      }
    }
  });

  /* 

  app.get("/app2/test", routes.test);

  app.get("/app2/folder/:folderid?", routes.folder);

  app.get("/app2/personas", routes.personas);

  app.get("/app2/personas/:id", routes.persona);

  app.get("/app2/album/:albumid", routes.album);

  app.get("/app2/pages/:albumid", routes.pages);
*/
  app.get("/app2/*", routes.nothing.handler);

  app.use(async (err, req, res, next) => {
    debugger;
    error(res, t("An error occured", req.query.lang) + ": " + err);
  });
}

export async function close() {}
