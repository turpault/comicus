import { Stats } from "fs";

export type Item = {
  path: string;
  uid: string;
  parentuid: string;
  text: string;
  type: "file" | "folder";
  kid: boolean;
  adult: boolean;
  inprogress?: boolean;
  status?: boolean;
  candownload?: boolean;
  ctime: Date;
  mtime: Date;
  out?: {
    title: string;
    file: string;
    mime: string;
  };
};

export type DecompressedData = {
  name: string;
  path: string;
  transient: boolean;
};

export type PagesWithExif = DecompressedData & { exif?: any; star?: boolean; faces?: any };

export type SimpleDBEntry = any & {
  _id?: string;
};

export type ResizeOptions = {
  name: string;
  width?: string;
  height?: string;
  orientation?: number;
  format: "webp" | "png" | "jpg";
};

export type StreamOptions = {
  name: string;
  imageEncoding?: "raw" | "base64" | "base64uri";
  format?: "webp" | "png" | "jpg";
  range?: any;
};

export type StatsWithName = Stats & { name: string; fullname: string };

export type Persona = {
  items: any;
  stats: any;
  info: any;
} & SimpleDBEntry;
