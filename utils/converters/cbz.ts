/*jslint node: true, indent:2 */
/*global require*/

import archiver from "archiver";
import { debug as d } from "debug";
import { createReadStream } from "fs";
import { Readable } from "stream";
import { reject } from "underscore";
import { DecompressedData, Item } from "../types";
const debug = d("comicus:cbz");

/*
    cbz.js
    Copyright (c) 2014 Jocelyn Turpault.
*/

export const convert = async function (item: Item, files: DecompressedData[]): Promise<Readable> {
  var archive = archiver("zip", {
    zlib: { level: 9 }, // Sets the compression level.
  });

  // listen for all archive data to be written
  archive.on("close", function () {
    debug(archive.pointer() + " total bytes");
    debug("archiver has been finalized and the output file descriptor has closed.");
  });

  // good practice to catch warnings (ie stat failures and other non-blocking errors)
  archive.on("warning", function (err) {
    if (err.code === "ENOENT") {
      debug("No such file", err);
    } else {
      debug("Can't compress", err);
    }
  });

  // good practice to catch this error explicitly
  archive.on("error", function (err) {
    debug("Can't compress", err);
    reject(err);
  });

  for (const file of files) {
    archive.append(createReadStream(file.path), { name: file.name });
  }

  // finalize the archive (ie we are done appending files but streams have to finish yet)
  archive.finalize();

  return archive;
};
