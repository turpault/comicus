import { debug as d } from "debug";
import { join } from "path";
import { config } from "../config";
import { SimpleDB } from "./simpledb";

const debug = d("comicus:express-cache");
export class ExpressCache {
  private db: SimpleDB;

  constructor() {
    const location = join(config.cache, "express-cache");

    //rimraf.sync(location);

    this.db = new SimpleDB(location, {
      evictionTime: 1000,
      mruSize: 1000,
    });
  }

  getNamespaceFromKey(key) {
    return key.match(/^[a-z0-9]*/)[0];
  }

  getKeyWithoutNamespace(key) {
    return key.match(/\:(.*)/)[1];
  }

  get(key, callback) {
    const start = new Date().getTime();
    // file deepcode ignore PromiseNotCaughtGeneral: <please specify a reason of ignoring this>
    this.db.get(key).then((data) => {
      const delay = new Date().getTime() - start;
      debug(data ? `Retrieved ${key} in ${delay} ms` : `Key ${key} not found in cache`);
      callback(data ? null : "Not found", data && data.val);
    });
  }

  set(key, val, expire, callback) {
    debug("Adding cache entry for key", key);
    this.db.add(key, { val });
    callback();
  }

  async close() {
    await this.db.close();
  }

  del(key, callback) {
    debugger;
    this.db.delete(key);
    callback();
  }

  flush(namespace, callback) {
    callback();
  }

  keys(namespace, callback) {
    this.db.keys().then((keys) =>
      callback(
        null,
        keys.filter((key) => this.getNamespaceFromKey(key) === namespace),
      ),
    );
  }

  ttl(key, callback) {
    this.db.get(key).then((data) => callback(null, data ? data.expire : undefined));
  }
}
