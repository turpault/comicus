#!/usr/bin/env node
/*jshint node: true, indent:2 */
/*global require*/

/*
    main.js
    Copyright (c) 2012-2014 Jocelyn Turpault.
*/

import bodyParser from "body-parser";
import compression from "compression";
import { debug as d } from "debug";
import express, { Express, NextFunction, Request, Response } from "express";
import { createServer, Server } from "http";
import morgan from "morgan";
import { join } from "path";
import { inspect, promisify } from "util";
import * as ssr from "./app2/ssr";
import { error } from "./app2/ssr";
import { close, config } from "./config";
import * as book from "./controllers/book";
import * as configModule from "./controllers/config";
import * as onlineDownloader from "./controllers/online-downloader";
import * as persona from "./controllers/persona";
import { personaInfo } from "./controllers/persona";
import * as system from "./controllers/system";
import "./i18n/translation";
import * as auth from "./middleware/auth";
import { resizeAndStream } from "./utils/streamers";
import { fileExists } from "./utils/fsutils";

const debug = d("comicus:book");

function asyncToExpress(fcts) {
  return fcts.map((f) => {
    if (f.constructor.name === "AsyncFunction") {
      return function (req: Request, res: Response, next: NextFunction) {
        f(req, res, next).catch((e) => {
          try {
            res.write(inspect(e));
            res.sendStatus(500);
            res.end();
          } catch (e) {
            // ignore, because headers might already have been sent
            try {
              res.end();
            } catch (e) {}
          }
          console.error(req.url, e);
        });
      };
    }
    return f;
  });
}

// Setup http routes
var app: Express & { getAsync: Function; postAsync: Function } = Object.assign(express(), {
  getAsync: function (route, ...fcts) {
    this.get(route, asyncToExpress(fcts));
  },
  postAsync: function (route, ...fcts) {
    this.post(route, asyncToExpress(fcts));
  },
});

app.use(morgan("dev"));
auth.install(app);
app.use(bodyParser.json());
app.use(compression());
app.all("/*", function (req: Request, res, next) {
  console.info("Serving", req.method, req.originalUrl);
  next();
});

app.use("/app", express.static(join("app", "dist")));
// Rewrite the url for application-specific assets
app.use("/favicon.ico", express.static(join("deviceassets", "favicon.ico")));
app.use("/deviceassets", express.static("deviceassets"));
app.getAsync("/resized/:image", async (req, res) => {
  let imagePath = join("deviceassets", req.params.image);
  let imageExists = await fileExists(imagePath);

  // Fallback on avatars
  if (!imageExists) {
    imagePath = join("avatars", req.params.image);
    imageExists = await fileExists(imagePath);
  }
  if (!imageExists) {
    imagePath = join("deviceassets", "default.png");
  }
  resizeAndStream(imagePath, { ...req.query, name: req.params.image }, res);
});
// Allow cors
app.all("/*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type, X-Requested-With, conce, signature");

  next();
});
app.options("/*", function (req, res) {
  res.header("Access-Control-Allow-Headers", "signature, conce, Content-Type, X-Requested-With");
  res.send(200);
  res.end();
});

app.use(personaInfo);

// Handle range
app.use((req, _res, next) => {
  // Populate options from query + headers
  const range = req.headers["content-range"] || req.headers["range"];
  req["options"] = {
    lang: "en",
    ...req.query,
    range,
  };
  next();
});

app.get("/", function (req, res) {
  debug("### Getting application ###", process.env.NODE_ENV);
  error(res, "Fail");
});

app.get("/config", (req, res) => {
  res
    .status(200)
    .send(
      `remoteConfig = { "name": "${config.name}", "title": "${config.name}", "mode":"${config.clientMode}" };`,
    );
});

/*app.get('/',redirectStatic);
app.get('/index.html',redirectStatic);
app.get('/debug.html',redirectStatic);
app.get('/index.htm',redirectStatic);
*/

/**
 * Error handler
 */
app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
  res.status(500);
  res.send({ error });
});

const modules = [system, persona, configModule, book, ssr, onlineDownloader];

export class ComicusServer {
  private _http: Server | undefined;
  private _https: Server | undefined;
  async init() {
    for (const ctrl of modules) {
      await ctrl.install(app);
      // The error handler must be before any other error middleware
    }
  }

  listen() {
    debug("Starting server...");
    this._http = createServer(app);
    this._http.listen(config.port);
  }

  async close() {
    try {
      for (const ctrl of modules) {
        if (ctrl.close) await ctrl.close();
      }
    } catch (err) {
      debug("Error while closing modules", err);
    }
    for (const server of [this._http, this._https, auth]) {
      console.info("Closing server", server);
      if (server && server.close) {
        await promisify(server.close)();
      }
    }
    await close();
  }
}

var server = new ComicusServer();
server.init().then(() => {
  debug("Server ready...");
  server.listen();
  (process as any).send &&
    typeof (process as any).send === "function" &&
    (process as any).send("ready");
});
