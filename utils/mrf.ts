/**
 * @fileinfo Manage recently accessed files
 */

import { memoizer, unlinkNoFail } from "./fsutils";
import { KeyedPQueue } from "./keyed-pqueue";
import { DecompressedData } from "./types";
import { decompress } from "./unarchivers/unarchiver";

// Keep 1000 recently accessed files in cache
const memoize = memoizer(1000);
export async function getArchiveFiles(archiveName, filter): Promise<DecompressedData[]> {
  return memoize(
    ["getArchiveFiles", archiveName],
    () => getArchiveFilesLocal(archiveName, filter),
    (data: DecompressedData[]) => {
      data.map((f) => unlinkNoFail(f.path)); // async
    },
  );
}

const deArchiverQueue = new KeyedPQueue({ concurrency: 3, timeout: 20000 });
let priority = 1;
async function getArchiveFilesLocal(archiveName, filter): Promise<DecompressedData[]> {
  priority++;
  return deArchiverQueue
    .add(
      archiveName,
      async () => {
        const fileData: DecompressedData[] = await decompress(archiveName).then((fileData) =>
          filter(fileData),
        );

        return fileData;
      },
      { atEnd: false },
    )
    .catch((e) => {
      console.error(`Error while dearchiving ${archiveName}`, e);
      throw e;
    });
}
