import archiver from "archiver";
import { load } from "cheerio";
import { debug as d } from "debug";
import { existsSync } from "fs";
import { get as httpGet } from "http";
import { get as httpsGet } from "https";
import { join } from "path";
import { filterImages } from "../utils/bookutils";
import { streamToBuffer, streamToFile } from "../utils/fsutils";
const debug = d("comicus:onlinedownloader");
import { URL } from "url";
import { setTimeout2 } from "../utils/timerreport";
import { rescanFiles } from "./book";
import { mkdir, stat } from "fs/promises";

async function sleep(delay: number) {
  return new Promise((resolve) => setTimeout2(resolve, delay * 1000));
}
async function getHTML(url: string): Promise<Buffer> {
  const fct = url.startsWith("https") ? httpsGet : httpGet;

  const redirs = [url];
  return new Promise((resolve, reject) => {
    fct(url, (res) => {
      if ([520].indexOf(res.statusCode!) >= 0) {
        debug(`${url}: Status code = ${res.statusCode}, will retry later...`);
        sleep(60).then(() => {
          getHTML(url).then(resolve).catch(reject);
        });
      } else if ([301, 302].indexOf(res.statusCode!) >= 0) {
        if (redirs.length > 10) {
          return reject("excessive 301/302 redirects detected");
        } else {
          if (redirs.indexOf(res.headers.location!) < 0) {
            redirs.push(res.headers.location!);
            getHTML(res.headers.location!).then(resolve).catch(reject);
            return;
          } else {
            reject("301/302 redirect loop detected");
            return;
          }
        }
      } else if (res.statusCode === 200) {
        streamToBuffer(res).then(resolve).catch(reject);
      } else {
        reject(`Status code = ${res.statusCode} : ${res.statusMessage}`);
      }
    });
  });
}

async function zip(entries: { name: string; data: Buffer }[], outFile: string): Promise<void> {
  var archive = archiver("zip", {
    zlib: { level: 0 }, // Sets the compression level.
  });

  // listen for all archive data to be written
  archive.on("close", function () {
    debug(archive.pointer() + " total bytes");
    debug("archiver has been finalized and the output file descriptor has closed.");
  });

  // good practice to catch warnings (ie stat failures and other non-blocking errors)
  archive.on("warning", function (err) {
    if (err.code === "ENOENT") {
      debug("No such file", err);
    } else {
      debug("Can't compress", err);
    }
  });

  // good practice to catch this error explicitly
  archive.on("error", function (err) {
    debug("Can't compress", err);
  });

  for (const file of entries) {
    archive.append(file.data, { name: file.name });
  }

  // finalize the archive (ie we are done appending files but streams have to finish yet)
  archive.finalize();

  // file deepcode ignore PT: <please specify a reason of ignoring this>
  return streamToFile(archive, outFile);
}

export async function downloadFromHTML(url: string, targetPath: string, log: (l: string) => void) {
  const b = await getHTML(url);
  const page = b.toString();

  if (targetPath === undefined || targetPath.length === 0) {
    const u = new URL(url);
    targetPath =
      "/Volumes/8TB_2/BDs/Manga/" +
      decodeURIComponent(
        u.pathname
          .split("/")
          .filter((a) => a && !["chapitres"].includes(a.toLowerCase()))
          .pop() as string,
      );
  }

  await stat(targetPath).catch((e) => {
    console.info(`Creating target path: ${targetPath}`);
    return mkdir(targetPath, { recursive: true });
  });

  const folders = Array.from(load(page)("a"));
  if (folders) {
    for (const folder of folders) {
      const f = folder.attribs["href"];
      if (f.startsWith("/")) {
        continue;
      }
      if (!f.endsWith("/")) {
        continue;
      }

      log(`Found chapter ${f}`);

      const target = join(targetPath, f.replace("/", "") + ".cbz");
      if (existsSync(target)) {
        continue;
      }
      const chapterUrl = url + f;
      const c = await getHTML(chapterUrl);
      const chapter = c.toString();
      const pages = Array.from(load(chapter)("a"));

      if (pages) {
        log(`${target} : ${pages.length} pages`);
        const items: { name: string; data: Buffer }[] = [];
        for (const page of pages) {
          const ref = page.attribs["href"];
          if (!filterImages(ref)) {
            continue;
          }
          const image = await getHTML(chapterUrl + ref);
          if (Buffer.isBuffer(image)) {
            items.push({ name: ref, data: image });
          }
        }
        log(`Saving ${target}`);
        await zip(items, target);
      }
    }
  }
}

export async function install(app) {
  // Download mangas every 24 hours
  app.get("/downloadFromHTML", (req, res) => {
    downloadFromHTML(req.query.url, req.query.destination, (l: string) => {
      debug(l);
      res.write(l + "\n");
    })
      .catch((e) => {
        res.write(`An error occured: ${e}`);
        console.warn(e);
      })
      .finally(() => {
        rescanFiles();
        res.end();
      });
  });
  // Example:
  // curl /downloadFromHTML?url=http://funquizzes.fun/uploads/manga/One%20Punch%20Man/Chapitres/&destination=/Volumes/8TB_2/BDs/Manga/One Punch Man/
}

export async function close() {}
