/*jslint node: true, indent:2 */
/*global require*/

import { Base64Encode } from "base64-stream";
import { debug as d } from "debug";
import diacritics from "diacritics";
import { Response } from "express";
import { createReadStream, statSync } from "fs";
import { ServerResponse } from "http";
import { basename } from "path";
import { Readable, Writable } from "stream";
import { resize } from "./images/image";
import { ResizeOptions, StreamOptions } from "./types";
const debug = d("comicus:streamers");
var _mimetypes = {
  jpg: "image/jpeg",
  jp2: "image/jpeg",
  jpeg: "image/jpeg",
  webp: "image/webp",
  gif: "image/gif",
  mov: "video/quicktime",
  mp4: "video/mp4",
  webm: 'video/webm;codecs="vp9"',
  cbz: "application/x-cbz",
  cbr: "application/x-cbr",
  pdf: "application/pdf",
  epub: "application/epub+zip",
  png: "image/png",
};

var sanitizeExpression = /[^a-zA-Z0-9 \.]/g;

function headers(name: string, mime: string, outstream: ServerResponse | Writable) {
  if (outstream instanceof ServerResponse) {
    debug("Stream to http response");
    outstream.setHeader("Content-Type", mime);
    var newName = diacritics.remove(basename(name)).replace(sanitizeExpression, "");
    debug("Generated filename:", newName);
    outstream.setHeader("Content-Disposition", 'inline; filename="' + newName + '"');
    outstream.statusCode = 200;
  }
}
function pipe(data: Buffer | Uint8Array | Readable | string, outstream: Writable): Writable {
  if (typeof data === "string") {
    return createReadStream(data).pipe(outstream);
  } else if (data instanceof Buffer) {
    return Readable.from([data]).pipe(outstream);
  } else if (data instanceof Uint8Array) {
    let stream = new Readable();
    stream.push(data);
    stream.push(null);
    return stream.pipe(outstream);
  } else {
    return data.pipe(outstream);
  }
}

function resolveStream(stream: Writable): Promise<void> {
  return new Promise((resolve, reject) => {
    stream.on("close", () => resolve());
    stream.on("error", (e) => reject(e));
  });
}

export async function raw(
  buffer: Buffer | Readable | Uint8Array | string,
  name: string,
  mime: string,
  outstream: ServerResponse | Writable,
): Promise<void> {
  headers(name, mime, outstream);

  return resolveStream(pipe(buffer, outstream));
}

export async function base64(
  buffer: Buffer | Readable | Uint8Array | string,
  name: string,
  mime: string,
  outstream: ServerResponse | Writable,
): Promise<void> {
  debug("Base64 streaming");
  headers(name, "text/plain", outstream);

  return resolveStream(pipe(buffer, new Base64Encode()).pipe(outstream));
}

export async function base64uri(
  buffer: Buffer | Readable | Uint8Array | string,
  name: string,
  mime: string,
  outstream: ServerResponse | Writable,
): Promise<void> {
  debug("Base64 URI streaming of", name);
  headers(name, "text/plain", outstream);
  outstream.write("data:" + mime + ";base64,");
  return resolveStream(pipe(buffer, new Base64Encode()).pipe(outstream));
}

function mimetypes(type) {
  return _mimetypes[type] || "application/octet-stream";
}

export async function resizeAndStream(
  fileName: string,
  options: ResizeOptions & StreamOptions,
  outstream: Response | Writable,
): Promise<void> {
  try {
    const fName = await resize(fileName, options);
    await streamFile(fName, options, outstream);
  } catch (e) {
    if ((outstream as any).status) {
      // status is only on response objects
      (outstream as Response).status(501).send("An error occured: " + e.message + "\n\n" + e.stack);
    } else {
      throw e;
    }
  }
}

export async function streamFile(
  fileName: string | Buffer,
  options: StreamOptions,
  outstream: Response | Writable,
): Promise<void> {
  const imageEncoding = options.imageEncoding || "raw";
  const format = options.format;
  const mime = mimetypes(format);

  // No format if there is a range
  if (outstream instanceof ServerResponse && options.range) {
    if (Buffer.isBuffer(fileName)) {
      throw new Error("Cannot do partial responses from buffers");
    }
    const res = outstream as ServerResponse;
    const stat = statSync(fileName);
    const fileSize = stat.size;
    const range = options.range;
    if (range) {
      const parts = range.replace(/bytes=/, "").split("-");
      const start = parseInt(parts[0], 10);
      const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
      const chunksize = end - start + 1;
      const file = createReadStream(fileName, { start, end });
      const head = {
        "Content-Range": `bytes ${start}-${end}/${fileSize}`,
        "Accept-Ranges": "bytes",
        "Content-Length": chunksize,
        "Content-Type": mime,
      };
      res.writeHead(206, head);
      file.pipe(res);
    } else {
      const head = {
        "Content-Length": fileSize,
        "Content-Type": mime,
      };
      res.writeHead(200, head);
      createReadStream(fileName).pipe(res);
    }
    return;
  }

  const streamers = {
    raw,
    base64,
    base64uri,
  };

  return streamers[imageEncoding](fileName, options.name, mime, outstream);
}
