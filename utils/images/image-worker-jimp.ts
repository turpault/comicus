/*jslint node: true, indent:2 */
/*global require*/

/*
    image-worker.js
    Copyright (c) 2014 Jocelyn Turpault.
*/
import { debug as d } from "debug";
import { readFile } from "fs/promises";
import Jimp from "jimp";
import { inspect } from "util";
import { safeWriteFile } from "../../utils/fsutils";
import { expose } from "../../utils/worker";
import { ResizeOptions } from "../types";
const debug = d("comicus:image-worker");

expose({
  imageResize: async (fileName: string, options: ResizeOptions, outFile: string): Promise<void> => {
    debug(`Start image resizing for file ${fileName}, options ${inspect(options)}`);
    const width =
        options && options.width !== undefined ? parseInt(options.width.toString()) : undefined,
      height =
        options && options.height !== undefined ? parseInt(options.height.toString()) : undefined,
      format = options?.format;
    const imageProcessingLabel = `${fileName} ${width} ${height}`;

    const debugImg = d("image:" + fileName);
    debugImg("Start");

    let t = imageProcessingLabel + ": read in Jimp";
    console.time(t);
    const buffer = await readFile(fileName);
    var jimp = await Jimp.read(buffer);
    console.timeEnd(t);
    debugImg("Read file from disk");

    if (options.orientation) {
      t = imageProcessingLabel + ": rotate";
      console.time(t);
      await jimp.rotate(options.orientation);
      console.timeEnd(t);
    }
    debugImg("Rotated");
    if (width || height) {
      t = imageProcessingLabel + ": resize";
      console.time(t);
      if (width && height) {
        await jimp.scaleToFit(width, height);
        debugImg("Rescaled");
      } else if (width || height) {
        await jimp.resize(width ?? Jimp.AUTO, height ?? Jimp.AUTO);
        debugImg("Resized");
      }

      console.timeEnd(t);
    }

    var mime = "image/jpeg";
    if (format) {
      switch (format) {
        case "webp":
          mime = "image/webp";
          break;
        case "png":
          mime = "image/png";
          break;
        default:
          break;
      }
    }

    t = imageProcessingLabel + ": get as buffer";
    console.time(t);
    // Hack: Clear exif data (to avoid re-orientation in clients)
    (jimp.bitmap as any).exifBuffer = undefined;
    const buf = await jimp.getBufferAsync(mime);
    debugImg(`Get buffer with format ${mime}`);
    await safeWriteFile(outFile, buf);
    debugImg(`Written output in ${outFile}`);

    console.timeEnd(t);
    return;
  },
});
