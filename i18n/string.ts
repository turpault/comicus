declare interface String {
  t(lang: string): string;
  replaceAll(ss: string, r: string): string;
}
