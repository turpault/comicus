#!/usr/bin/env bash  
echo "Decompressing $1 to $2"
infile=$1
outdir=$2
mkdir -p "$outdir"
unrar x -y "$infile" "$outdir"
#clear non unicode chars
"`dirname "$0"`/clearfilenames.sh" "$outdir"
