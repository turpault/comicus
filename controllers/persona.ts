import { debug as d } from "debug";
import { basename, extname, join } from "path";
import { clone, compact, defaults, extend, pick, pluck, sortBy } from "underscore";
import { Item, SimpleDBEntry } from "../utils/types";
import { config } from "../config";
import { basicauth, basicorsecretauth, secretAuth } from "../middleware/auth";
import { sanitizeTitle } from "../utils/bookutils";
import { filesFromFolder, uuid } from "../utils/fsutils";
import { resizeAndStream } from "../utils/streamers";
import { SimpleDB } from "../utils/simpledb";
import { itemFromId } from "./book";
const debug = d("comicus:persona");

var personaDB = new SimpleDB(join(config.db, "persona"));

let avatarList: { uid: string; name: string; fullname: any; thumb: string }[];
let defaultAvatar: string;
let defaultInfo;

function fileNameToAvatar(fName) {
  return basename(fName, extname(fName));
}

export function newPersona(info) {
  if (!info.uid) {
    info.uid = uuid();
  }
  var persona: SimpleDBEntry = {
    items: {},
    stats: {},
    info: defaults(info, defaultInfo),
  };
  persona = personaDB.add(persona.info.uid, persona);

  return persona;
}
personaDB.keys().then((users) => {
  if (compact(users).length === 0) {
    newPersona({});
  }
});

export async function values() {
  return personaDB.values();
}

export async function items(personaid, filt, sort) {
  var persona = await get(personaid);
  if (!persona) {
    throw new Error("Persona not found");
  }

  var reverseSort = false;
  if (sort && sort.substr(0, 1) == "-") {
    sort = sort.substr(1);
    reverseSort = true;
  }
  var filtered: ItemWithUserData[] = [];
  for (const id in persona.items) {
    if (filt(persona.items[id])) {
      const item = itemFromId(id);
      if (item) {
        filtered.push(extendWithUserInfo(item, persona.items[id]));
      }
    }
  }
  let sorted = sortBy(filtered, sort);
  if (reverseSort) sorted = sorted.reverse();
  return sorted;
}

export async function filter(personaid, files) {
  if (!personaid) {
    return files;
  }
  var persona = await get(personaid);
  if (!persona) {
    throw new Error("Persona not found");
  }
  return files.filter(function (item) {
    if (item.adult && !persona.info.adult) return false;
    if (persona.info.kid && !item.kid) return false;
    return true;
  });
}

export async function get(personaid) {
  return (await personaDB.get(personaid)) || (await personaDB.get(decodeURIComponent(personaid)));
}

// Itemdata (folder of file)
export async function setItemData(personaid, uid, obj) {
  var persona = await get(personaid);
  if (!persona) {
    debug(`Personal {personaid} not found`);
    throw new Error("Persona not found");
  }
  persona.items[uid] = persona.items[uid] || {};
  for (const k of Object.keys(obj)) {
    if (obj[k] === "toggle") {
      persona.items[uid][k] = !persona.items[uid][k];
    } else if (obj[k] !== undefined) {
      persona.items[uid][k] = obj[k];
    }
  }
  personaDB.add(personaid, persona);
  return persona.items[uid];
}

type ItemWithUserData = Item & { [userkey: string]: boolean };

function extendWithUserInfo(item: Item, userData): ItemWithUserData {
  return extend(clone(item), userData);
}

export async function merge(personaid, item: Item[]): Promise<ItemWithUserData[]> {
  if (!personaid) {
    return item as ItemWithUserData[];
  }
  var _mergeSingle = function (originalItem, personaItems) {
    return extendWithUserInfo(originalItem, personaItems[originalItem.uid]);
  };
  var persona = await get(personaid);
  if (!persona) {
    throw new Error("Persona not found");
  }
  return item.map(function (i) {
    return _mergeSingle(i, persona.items);
  });
}

export async function personaInfo(req, res, next) {
  var persona = req.query.persona && (await get(req.query.persona));
  if (persona) {
    req.persona = persona;
    req.personaid = persona.info.uid;
  }
  next();
}

export async function avatars() {
  // enumerate avatars
  // known extensions - todo: Check filetype
  var extensions = [".png", ".jpg"];
  const files = sortBy(await filesFromFolder(join("avatars")), ["name"]);
  var images = files
    .filter(function (file) {
      return extensions.includes(extname(file.name).toLowerCase());
    })
    .map(function (item) {
      const uid = sanitizeTitle(item.name);
      return {
        uid,
        name: fileNameToAvatar(item.name),
        fullname: item.fullname,
        thumb: `/avatar/${uid}`,
      };
    });
  return images;
}

export function personaAuth(req, res, next) {
  if (!req.persona) {
    console.error(`Persona not found (id=${req.query.personaid}`);
    res.redirect("/app2");
    return;
  }
  if (next) {
    next();
  }
}

export async function install(app) {
  avatarList = await avatars();
  defaultAvatar = avatarList[0].uid;

  defaultInfo = {
    name: "*",
    avatarid: defaultAvatar,
    kid: false,
    admin: false,
    adult: false,
    uid: uuid(),
  };

  app.getAsync("/personas", basicorsecretauth, async (req, res) => {
    await app.resultRaw(req, res, pluck(personaDB.values(), "info"));
  });

  app.getAsync("/personalist", secretAuth, async (req, res) => {
    await app.result(req, res, pluck(personaDB.values(), "info"));
  });

  app.getAsync("/bookinfo", secretAuth, async (req, res) => {
    var filters = pick(req.query, ["type", "read", "like"]);
    const data = await items(req.personaid, filters, req.query.sort);
    await app.result(req, res, data);
  });

  app.getAsync("/bookinfo/:uid", secretAuth, async (req, res) => {
    if (!req.persona) {
      throw new Error("Persona not found");
    } else {
      var item = req.persona.items[req.params.uid];
      if (!item) {
        throw new Error("Item not found");
      }
      return app.result(req, res, item);
    }
  });

  app.postAsync("/bookinfo/:uid/:key", personaAuth, async (req, res) => {
    const data = await setItemData(req.personaid, req.params.uid, {
      [req.params.key]: req.body.value,
    });
    await app.result(req, res, { value: data[req.params.key] });
  });

  app.postAsync("/bookinfo/:uid", secretAuth, personaAuth, async (req, res) => {
    const data = await setItemData(req.personaid, req.params.uid, req.body);
    await app.result(req, res, data);
  });

  app.getAsync("/persona/:persona", async (req, res) => {
    var persona = req.params.persona && (await get(req.params.persona));
    console.info("PERSONA ", persona);
    if (!persona) {
      throw new Error("Persona not found");
    } else {
      await app.resultRaw(req, res, persona.info);
    }
  });

  app.postAsync("/persona", basicauth, async (req, res) => {
    var p = newPersona(req.body);
    return app.resultRaw(req, res, p.info, 201);
  });

  app.postAsync("/persona/:persona", async (req, res) => {
    var persona = req.params.persona && (await get(req.params.persona));
    if (!persona) throw new Error("Persona not found");

    persona.info = defaults(req.body, persona.info);
    personaDB.update(persona);
    await app.result(req, res, persona.info);
  });

  app.delete("/persona/:persona", basicauth, async (req, res) => {
    var persona = req.params.persona && (await get(req.params.persona));
    if (!persona) throw new Error("Persona not found");

    personaDB.delete(persona);
    await app.resultRaw(req, res);
  });

  app.getAsync("/avatars", async (req, res) => {
    await app.result(req, res, avatarList);
  });

  app.getAsync("/avatar/:avatar", async (req, res) => {
    const result = avatarList;
    const avatar = result.find((a) => a.uid === req.params.avatar);
    let fName;
    if (!avatar) {
      fName = join("avatars", "default.png");
    } else {
      fName = avatar.fullname;
    }
    return resizeAndStream(fName, { ...req.query, name: fName }, res);
  });
}
export async function close() {
  return personaDB.close();
}
