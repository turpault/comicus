/**
 * Returns a promise that resolves after the given delay
 * @param delay the delay in seconds
 * @returns
 */
export async function sleep(delay: number) {
  return new Promise((resolve) => setTimeout(resolve, delay * 1000));
}

export function sortByKey<T>(array: T[], key: keyof T) {
  array.sort((a, b) => (a[key] < b[key] ? -1 : a[key] > b[key] ? 1 : 0));
}

export function uuid(): string {
  return new Date().getTime().toString(36) + Math.random().toString(36).slice(2);
}

const debounceFcts = new Map<Function, number>();
export function debounce(f: Function, delay?: number) {
  delay = delay ? delay : 1000;
  if (debounceFcts.has(f)) {
    // debounceFcts.set(f, Date.now() + delay);
  } else {
    debounceFcts.set(f, Date.now() + delay);
    (async () => {
      while (true) {
        const target = debounceFcts.get(f);
        const now = Date.now();
        if (!target || target <= now) {
          debounceFcts.delete(f);
          f();
          break;
        } else {
          await sleep((target - now + 100) / 1000);
        }
      }
    })();
  }
}

export function range(from: number, to: number): number[] {
  const dir = from < to ? 1 : -1;
  const res: Array<number> = [];
  while (from != to) {
    res.push(from);
    from += dir;
  }
  res.push(from);
  return res;
}

export function flattenObject(ob: any) {
  var toReturn: any = {};

  for (var i in ob) {
    if (!ob.hasOwnProperty(i)) continue;

    if (typeof ob[i] == "object" && ob[i] !== null) {
      var flatObject = flattenObject(ob[i]);
      for (var x in flatObject) {
        if (!flatObject.hasOwnProperty(x)) continue;
        toReturn[x] = flatObject[x];
      }
    } else if (Buffer.isBuffer(ob[i])) {
      toReturn[i] = ob[i].toString({ encoding: "utf8" });
    } else {
      toReturn[i] = ob[i];
    }
  }
  return toReturn;
}

class Mutex {
  constructor() {
    this.current = Promise.resolve();
    this.nest = 0;
  }
  private current: Promise<void>;
  private nest: number;
  lock() {
    let _resolve: Function;
    const p = new Promise<void>((resolve) => {
      _resolve = () => {
        this.nest--;
        return resolve();
      };
    });
    // Caller gets a promise that resolves when the current outstanding
    // lock resolves
    const rv = this.current.then(() => _resolve);
    // Don't allow the next request until the new promise is done
    this.current = p;
    // Return the new promise
    this.nest++;
    return rv;
  }
  locked() {
    return this.nest > 0;
  }
}
const locks: Map<string, Mutex> = new Map();

function pruneLocks() {
  // Prune unused mutexes
  for (const l in locks) {
    if (!locks.get(l)!.locked()) {
      locks.delete(l);
    }
  }
}

export async function lock(label: string): Promise<Function> {
  pruneLocks();
  if (!locks.has(label)) {
    locks.set(label, new Mutex());
  }
  return locks.get(label)!.lock();
}

export function lockedLocks(): string[] {
  return Array.from(locks)
    .filter((l) => l[1].locked())
    .map((l) => l[0]);
}

export function sanitize(s: string) {
  // TODO
  return s;
}
