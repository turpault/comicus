/*jslint node: true, indent:2 */
/*global require*/

/*
    pdf.ts
    Copyright (c) 2021 Jocelyn Turpault.
*/

var PDFDocument = require("pdfkit");

import imageinfo from "imageinfo";
import { Readable } from "stream";
import { DecompressedData, Item } from "../types";
import { readFile } from "fs/promises";

export async function convert(item: Item, entries: DecompressedData[]): Promise<Readable> {
  var doc = new PDFDocument({ autoFirstPage: false, margin: 0 });
  for (const entry of entries) {
    const data = await readFile(entry.path);
    const info = imageinfo(data);
    if (info.width && info.height) {
      doc.addPage({ margin: 0, size: [info.width, info.height] }).image(data);
    }
  }

  doc.end();
  return doc;
}
