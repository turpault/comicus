/* jshint node:true */
import { config } from "../config";
import { basicauth } from "../middleware/auth";

export async function install(app) {
  app.get("/config", basicauth, function (req, res) {
    app.resultRaw(req, res, config);
  });
}

export async function close() {}
