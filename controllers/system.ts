/*jslint node: true, indent:2 */
/*global require*/

import { existsSync, mkdirSync } from "fs";
import { join } from "path";
import { config } from "../config";
import { writeFile } from "fs/promises";
/*
    system.js
    Copyright (c) 2012-2014 Jocelyn Turpault.
*/

export function ping() {
  return { name: config.name, clientConfig: config.clientMode };
}

export async function log(contents, remoteAddress) {
  var clientlogpath = join(config.tmp, "clientlogs");
  var title = contents.title || "No title";
  var version = contents.version || "No version";
  var sanitizedTitle = title.replace(/[^a-z0-9 ]/gi, " ").substr(0, 100);
  var folder = join(clientlogpath, version);
  if (!existsSync(folder)) {
    mkdirSync(folder, { recursive: true });
  }
  contents.date = new Date();
  contents.ip = remoteAddress;

  var filename = sanitizedTitle + " on " + new Date().toISOString().replace(":", " ") + ".json";

  return writeFile(join(folder, filename), JSON.stringify(contents, undefined, 2));
}

// Setup
export async function install(app) {
  app.all("/ping", async (req, res) => {
    await app.result(req, res, ping());
  });

  app.postAsync("/log", async (req, res) => {
    console.info(`Logs from connection ${req.connection.remoteAddress}`);
    console.info(req.body);
    await app.result(req, res, log(req.body, req.connection.remoteAddress));
  });
}

export async function close() {}
