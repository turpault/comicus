#!/usr/bin/env bash

./deploy.sh $*
(
  cordova prepare
  cordova build ios --device --release
)
