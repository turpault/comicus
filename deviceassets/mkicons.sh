#!/usr/bin/env bash 
background="black"

cd "`dirname "$0"`"
mkdir -p resized
for app in bds comics photos
do
convert icon-$app.png -resize 32x32 resized/favicon-$app.ico
for i in 100 114 120 144 152 180 29 36 40 48 50 57 58 60 72 76 80 96 180 152
  do
    echo "Creating $i"
    convert icon-$app.png -resize ${i}x${i} "resized/icon-$app-${i}x${i}.png"
#    pngcrush -brute -ow "$i"
  done

for i in 1024x768 1242x2208 1280x720 1536x2048 200x320 2048x1536 2208x1242 320x200 320x480 480x320 480x800 640x1136 640x960 720x1280 750x1334 768x1024 800x480 1136x640 2436x1125 1792x828 828x1792 1334x750 1242x2688 2208x1242 1125x2436 1242x2208 2732x2048 2688x1242 2224x1668 750x1334 2048x2732 2388x1668 1668x2224 640x1136 1668x2388 2048x1536 1536x2048
  do
    echo "Creating $i"
    convert splash-$app.png -resize $i -background $background -gravity center -extent $i "resized/splash-$app-$i.png"
#    pngcrush -brute -ow "$i"
  done

convert splash-$app.png -resize 1334x1334 resized/Default@2x~iphone~anyany-$app.png
convert splash-$app.png -resize 2732x2732 resized/Default@2x~ipad~anyany-$app.png
done

exit 0
# Border version, but does not work well (flashes)
for i in resized/splash*.png
  do
    echo "Creating $i"
    res=`identify -format "%wx%h" "$i"`
    IFS='x' read -a siz <<< "$res"
    height=${siz[1]}
    width=${siz[0]}
    echo "$i : $width x $height"
    border=`echo "$height * 0.06" | bc`
    echo "Border : $border"

    convert -size $res xc: -fill "#067BC2" -draw "rectangle 0,0,$width,$border" -fill "#f0f0f0" -draw "rectangle 0,$border,$width,$height" -blur 10  "$i"
    #convert splash.png -resize $res -background $background -gravity center -extent $res "$i"
#    pngcrush -brute -ow "$i"
