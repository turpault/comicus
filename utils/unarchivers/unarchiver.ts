/*
    unarchiver.js
    Copyright (c) Jocelyn Turpault
*/

import d from "debug";
import { mkdir } from "fs/promises";
import { basename, extname, join } from "path";
import { config } from "../../config";
import { Handler, childProcess } from "../../utils/worker";
import { setInterval2 } from "../timerreport";
import { DecompressedData } from "../types";
const debug = d("comicus:unarchiver");

const pool: Handler[] = [];

export async function decompress(in_file: string): Promise<DecompressedData[]> {
  const start = new Date().getTime();
  debug(`Start decompressing ${in_file}`);
  let worker;
  if (pool.length > 0) {
    worker = pool.shift()!;
  } else {
    const w = childProcess(join(__dirname, "./unarchiver-worker"), () => {
      debug("Child process was killed");
    });
    worker = w;
  }
  try {
    const outFolder = join(config.tmp, "decompressedArchives", basename(in_file, extname(in_file)));
    await mkdir(outFolder, { recursive: true });
    const res = await worker.decompress(in_file, outFolder);
    debug(`Stop decompressing ${in_file}: ${new Date().getTime() - start} ms`);
    pool.push(worker);
    return res;
  } catch (e) {
    console.warn(`Exception while uncompressing : ${e.message}`);
    throw e;
  }
}

setInterval2(async () => {
  // Prune the pool queue from time to time
  if (pool.length > 0) {
    debug("Terminating file decompression worker");
    const worker = pool.shift()!;
    worker.terminate();
  }
}, 60000);
