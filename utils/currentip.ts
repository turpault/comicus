import { request } from "http";
const options = {
  hostname: "ipecho.net",
  path: "/plain",
};
import { debug as d } from "debug";
const debug = d("comicus:detect");

export const detect = async function () {
  return new Promise((resolve, reject) => {
    try {
      var req = request(options, (res) => {
        res.on("data", function (chunk) {
          resolve(String(chunk));
        });
      });
      req.on("error", function (err) {
        debug("Error : " + err);
        reject(err);
      });

      req.end();
    } catch (e) {
      reject("Exception while querying ip :" + e.message);
    }
  });
};
