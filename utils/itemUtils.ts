/*jslint node: true, indent:2 */
/*global require*/

/*
    persona.js
    Copyright (c) 2012-2014 Jocelyn Turpault.
*/
//User Apis
import { sortBy, toArray } from "underscore";
import { exists } from "../controllers/book";

export function filter(items, filt, sort) {
  var reverseSort = false;
  if (sort && sort.substr(0, 1) == "-") {
    sort = sort.substr(1);
    reverseSort = true;
  }
  var filtered = toArray(items).filter(function (item) {
      var r = filt.name in item && (filt.value === undefined || item[filt.name] == filt.value);
      var res = filt.invert ? !r : r;
      return res;
    }),
    filteredExists = filtered.filter(function (item) {
      return exists(item.uid);
    }),
    sorted = sortBy(filteredExists, sort);
  if (reverseSort) sorted = sorted.reverse();
  return sorted;
}
