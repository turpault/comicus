/*
    fsutils.js
    Copyright (c) Jocelyn Turpault
*/

import concat from "concat-stream";
import d from "debug";
import { createWriteStream, PathLike, Stats } from "fs";
import { lstat, readdir, rename, stat, unlink, writeFile } from "fs/promises";
import { ClientRequest } from "http";
import { join, relative } from "path";
import { Readable } from "stream";
import { StatsWithName } from "./types";
import { lock } from "./mutex";
const debug = d("comicus:fsutils");

export const uuid = function () {
  var t,
    p =
      Math.random().toString(16).substr(2, 8) +
      "-" +
      (t = Math.random().toString(16).substr(2, 8)).substr(0, 4) +
      "-" +
      t.substr(4, 4) +
      (t = Math.random().toString(16).substr(2, 8)).substr(0, 4) +
      "-" +
      t.substr(4, 4) +
      Math.random().toString(16).substr(2, 8);
  return p;
};

export async function fileExists(path: string): Promise<boolean> {
  return fsMemoizer(["stat", path], () =>
    stat(path)
      .then(() => true)
      .catch(() => false),
  );
}

export async function unlinkNoFail(p: PathLike): Promise<void> {
  return unlink(p).catch((e) => debug(`An error occured while deleting file ${p}: ${e.message}`));
}

/**
 * Returns a memoizer function that will cache the results of the data function
 * The cache is scoped to the returned memoize function (multiple memoizer functions will have different caches)
 * @returns
 */
export function memoizer(maxCount = 5000) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const cache: { [key: string]: any } = {};
  const allKeys: string[] = [];
  return function memoize<T>(
    keys: string[],
    data: () => Promise<T>,
    evict?: (T) => void,
  ): Promise<T> {
    const k = keys.join(",");
    if (cache[k]) {
      // move key at the end of the list
      allKeys.splice(allKeys.indexOf(k), 1);
      allKeys.push(k);
      return cache[k];
    }
    if (allKeys.length > maxCount) {
      const old = allKeys.shift() as string;
      if (evict) {
        evict(cache[old]);
      }
      delete cache[old];
    }
    cache[k] = data();
    allKeys.push(k);
    return cache[k];
  };
}

const fsMemoizer = memoizer();
export async function allStats(folder): Promise<StatsWithName[]> {
  const files = await readdir(folder).catch((e) => {
    console.error(e);
    debugger;
    return [];
  });

  return Promise.all(
    files.map(async (name) => {
      const fullname = join(folder, name);
      const stat = await fsMemoizer(["lstat", fullname], () =>
        lstat(fullname).catch((e) => {
          debugger;
          debug(`An error occured while trying to stat ${name}: ${e.message}`);
          return {
            isDirectory: () => false,
            isFile: () => false,
            ctime: new Date(),
            mtime: new Date(),
          } as Stats;
        }),
      );
      return Object.assign(stat, { fullname, name });
    }),
  );
}

export const filesFromFolder = async function (folder) {
  var newfiles: StatsWithName[] = [];
  const allstats = await allStats(folder);
  allstats.forEach(function (stat, index) {
    if (!stat.isDirectory()) newfiles.push(stat);
  });
  return newfiles;
};

export async function filesFromFolderRecursive(topFolder) {
  var filesInFolder: string[] = [];
  const promises: Promise<any>[] = [];
  var queueFolder = async (absfolder) => {
    debug("Walking", absfolder);
    let files = await allStats(absfolder);

    // Ignore entries beginning with a .
    files = files.filter(function (file) {
      return file.name.substr(0, 1) != ".";
    });

    var filesAbsPath = files.map(function (file) {
      return join(absfolder, file.name);
    });

    files.forEach((stat, index) => {
      if (stat.isDirectory()) {
        promises.push(queueFolder(filesAbsPath[index]));
      } else {
        filesInFolder.push(relative(topFolder, filesAbsPath[index]));
      }
    });
  };

  promises.push(queueFolder(topFolder));
  while (promises.length !== 0) {
    await promises.shift();
  }
  return filesInFolder;
}

export async function streamToFile(stream: Readable, filename: string): Promise<void> {
  return new Promise((resolve, reject) => {
    const pipe = stream.pipe(createWriteStream(filename));
    pipe.on("close", () => resolve());
    pipe.on("end", () => resolve());
    pipe.on("error", (err) => reject(err));
  });
}

export async function streamToBuffer(stream: Readable | ClientRequest): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    const pipe = stream.pipe(concat(resolve));
    pipe.on("error", (err) => reject(err));
  });
}

export async function safeWriteFile(fileName: string, data: any) {
  const unlock = await lock("safeWriteFile: " + fileName);
  try {
    const tmp = fileName + ".tmp";
    await writeFile(tmp, data);
    await rename(tmp, fileName);
  } catch (e) {
    console.warn(`Could not save ${fileName}: ${e}`);
  } finally {
    unlock();
  }
}
