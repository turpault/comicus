#!/usr/bin/env bash
version=1.5
hash=`git rev-parse --short HEAD`
d=`date  +'%Y%m%d%H%M'`
REVISION=`git log -n 1 --pretty=format:"%H"`


prod=0
build=0

for i in "$@" ; do
  if [[ "$i" = "-prod" ]] ; then prod=1 ; fi
  if [[ "$i" = "--prod" ]] ; then prod=1 ; fi
  if [[ "$i" = "-p" ]] ; then prod=1 ; fi

done

if (( prod == 1 ))
  then
  echo "Production build"
  env="production"
  else
  env="development"
  fi

echo "window.appversion=\"$version-$hash-$d\";" >app/version.js
echo "window._githash=\"$REVISION\";" >>app/version.js
echo "window._environment=\"$env\";" >>app/version.js

npm install
npm run build
npm run buildSetup
bash deviceassets/mkicons.sh
