/*
    epub.js
    Copyright (c) Jocelyn Turpault
*/

import archiver from "archiver";
import { debug as d } from "debug";
import { extname, sep } from "path";
import { Readable } from "stream";
import { clone, extend } from "underscore";
import { pageSortAndFilter } from "../bookutils";
import { uuid } from "../fsutils";
import { DecompressedData, Item } from "../types";
import "../../i18n/translation";
import { replaceAll } from "../../i18n/translation";
import { readFile } from "fs/promises";
const debug = d("comicus:epub");

// Every EPUB file has a mimetype file in the root. This is all it has.
var _mimeType = "application/epub+zip";

// container.xml is a required file for EPUB
var _containerXml =
  "<?xml version='1.0' encoding='UTF-8' ?>\n" +
  "<container version='1.0' xmlns='urn:oasis:names:tc:opendocument:xmlns:container'>\n" +
  "<rootfiles>\n" +
  "<rootfile full-path='volume.opf' media-type='application/oebps-package+xml' />\n" +
  "</rootfiles>\n" +
  "</container>\n";

// OPF is another requierd file for EPUB.  It contains a list of all of the files in the book, as well as a 'spine'
var _opf =
  "<?xml version='1.0' encoding='UTF-8' ?>\n" +
  "<package version='2.0' unique-identifier='PrimaryID' xmlns='http://www.idpf.org/2007/opf'>\n" +
  "<metadata xmlns:dc='http://purl.org/dc/elements/1.1/' xmlns:opf='http://www.idpf.org/2007/opf'>\n" +
  "<dc:title>##Title##</dc:title>\n" +
  "<dc:creator opf:role='aut'>##Author##</dc:creator>\n" +
  "<dc:identifier id='PrimaryID' opf:scheme='URN'>##UUID##</dc:identifier>\n" +
  "<dc:language>en</dc:language>\n" +
  "<dc:publisher>BDReader</dc:publisher>\n" +
  '<meta name="cover" content="cover-image"/>\n' +
  "</metadata>\n" +
  "##manifest## \n" +
  "##spinefiles##\n" +
  "<guide>\n" +
  '<reference href="cover.xhtml" type="cover" title="Cover"/>\n' +
  "</guide>\n" +
  "</package>\n";

// NCX is another required file. Honestly no idea what it's used for but here it is anyway
var _ncx =
  "<?xml version='1.0' encoding='UTF-8'?>\n" +
  "<!DOCTYPE ncx PUBLIC '-//NISO//DTD ncx 2005-1//EN'\n" +
  "'http://www.daisy.org/z3986/2005/ncx-2005-1.dtd'>\n" +
  "<ncx xmlns='http://www.daisy.org/z3986/2005/ncx/' version='2005-1' xml:lang='en'>\n" +
  "<head>\n" +
  "<meta name='dtb:depth' content='0'/>\n" +
  "<meta name='dtb:totalPageCount' content='##PAGECOUNT##'/>\n" +
  "<meta name='dtb:maxPageNumber' content='##PAGECOUNT##'/>\n" +
  "<meta name='dtb:uid' content='##UUID##'/>\n" +
  "</head>\n" +
  "<docTitle>\n" +
  "<text>##Title##</text>\n" +
  "</docTitle>\n" +
  "<docAuthor>\n" +
  "<text>##Author##</text>\n" +
  "</docAuthor>\n" +
  "<navMap>\n" +
  '<navPoint id="navPoint1" playOrder="1">\n' +
  "<navLabel><text>Cover</text></navLabel>\n" +
  '<content src="cover.xhtml"/>\n' +
  "</navPoint>\n" +
  "</navMap>\n" +
  "</ncx>  \n";

var _cover =
  "<?xml version='1.0' encoding='utf-8'?>\n" +
  '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">' +
  "<head>" +
  "<title>Cover</title>" +
  '<style type="text/css"> img { max-width: 100%; } </style>' +
  "</head>" +
  "<body>" +
  '<div id="cover-image">' +
  '<img src="##COVER##" alt="##Title##"/>' +
  "</div>" +
  "</body>" +
  "</html>";

type epubPages = {
  name: string;
  data: Buffer | string;
  options?: {
    store: boolean;
  };
};
export async function convert(item: Item, entries: DecompressedData[]): Promise<Readable> {
  var uid = uuid();

  // Resort pages
  entries = pageSortAndFilter(entries);

  const pages: epubPages[] = [];

  // Change names to have all images in a single folder
  let page = 0;
  for (const entry of entries) {
    pages.push({
      name: "" + page++ + extname(entry.name).toLowerCase(),
      data: await readFile(entry.path),
    });
  }

  var pathComponents = item.path.split(sep);
  var bookTitle = item.text;

  var bookAuthor = pathComponents[0];
  var bookDate = new Date().toString();
  var coverImage = pages[0];
  var fileList = pages.slice(1);

  var UUID = uid;
  var assets = generateHTMLFiles(coverImage, fileList);
  var htmlFiles = assets.pages;

  var ncx = clone(_ncx);

  ncx = replaceAll(ncx, "##PAGECOUNT##", String(fileList.length)).toString();
  ncx = replaceAll(ncx, "##UUID##", UUID).toString();
  ncx = replaceAll(ncx, "##Title##", bookTitle).toString();
  ncx = replaceAll(ncx, "##Author##", bookAuthor).toString();

  var opf = clone(_opf);
  opf = replaceAll(opf, "##Title##", bookTitle).toString();
  opf = replaceAll(opf, "##Author##", bookAuthor).toString();
  opf = replaceAll(opf, "##Date##", bookDate).toString();
  opf = replaceAll(opf, "##UUID##", UUID).toString();
  opf = replaceAll(opf, "##manifest##", assets.manifest).toString();
  opf = replaceAll(opf, "##spinefiles##", assets.spine).toString();

  var cover = clone(_cover);
  cover = replaceAll(cover, "##Title##", bookTitle).toString();
  cover = replaceAll(cover, "##COVER##", coverImage.name).toString();
  var tocs: epubPages[] = [];
  tocs.push(
    {
      name: "mimetype",
      data: _mimeType,
      options: {
        store: true,
      },
    },
    {
      name: "cover.xhtml",
      data: cover,
    },
    {
      name: "index.ncx",
      data: ncx,
    },
    {
      name: "META-INF/container.xml",
      data: _containerXml,
    },
    {
      name: "volume.opf",
      data: opf,
    },
  );

  var archive = archiver("zip", {
    zlib: { level: 9 }, // Sets the compression level.
  });

  // listen for all archive data to be written
  archive.on("close", function () {
    debug(archive.pointer() + " total bytes");
    debug("archiver has been finalized and the output file descriptor has closed.");
  });

  // good practice to catch warnings (ie stat failures and other non-blocking errors)
  archive.on("warning", function (err) {
    if (err.code === "ENOENT") {
      debug("No such file", err);
    } else {
      debug("Can't compress", err);
    }
  });

  // good practice to catch this error explicitly
  archive.on("error", function (err) {
    debug("Can't compress", err);
  });

  const allAssets = [...tocs, ...htmlFiles, ...pages];
  for (const file of allAssets) {
    archive.append(file.data, extend({ name: file.name }, file.options));
  }

  // finalize the archive (ie we are done appending files but streams have to finish yet)
  archive.finalize();

  return archive;
}

function generateHTMLFiles(
  cover: { name: string },
  files: { name: string }[],
): {
  manifest: string;
  spine: string;
  pages: epubPages[];
} {
  const pages: epubPages[] = [];
  var manifest = "<manifest>\n";
  var spine = "<spine toc='ncx'>\n";

  manifest += "<item id='ncx' href='index.ncx' media-type='application/x-dtbncx+xml'/>\n";
  manifest += "<item id='cover' href='cover.xhtml' media-type='application/xhtml+xml'/>\n";
  spine += "<itemref idref='cover'/>\n";
  manifest +=
    "<item id='cover-image' href='" +
    cover.name +
    "' media-type='" +
    GetMediaType(cover.name) +
    "'/>\n";
  for (var i = 0; i < files.length; i++) {
    var idx = i + 1;
    var htmlName = "id" + idx;
    var htmlFileName = "id" + idx + ".html";
    var file = files[i];
    manifest +=
      "<item id='" +
      htmlName +
      "' href='" +
      htmlName +
      ".html" +
      "' media-type='application/xhtml+xml'/>\n";
    manifest +=
      "<item id='" +
      htmlName +
      "img' href='" +
      file.name +
      "' media-type='" +
      GetMediaType(file.name) +
      "'/>\n";
    spine += '<itemref idref="' + htmlName + '"/>\n';

    var od =
      "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.1//EN'\n " +
      "'http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd'>\n" +
      "<html xmlns='http://www.w3.org/1999/xhtml'>\n" +
      "<head>\n" +
      "<meta http-equiv='content-type' content='text/html; charset=UTF-8'/>\n" +
      "<meta name='title' content=''/>\n" +
      "<meta name='creator' content='ComicToEPUB'/>\n" +
      "<meta name='language' content='en'/>\n" +
      "<title></title>\n" +
      "</head>\n" +
      "<body style='margin: 0'>\n" +
      "<div>\n" +
      '<img style="width:100%;" alt=\'' +
      file.name +
      "' src='" +
      file.name +
      "' />\n" +
      "</div>\n" +
      "</body>\n" +
      "</html>\n";
    pages.push({
      name: htmlFileName,
      data: od,
    });
  }
  manifest += "</manifest>\n";
  spine += "</spine>\n";

  return {
    manifest,
    spine,
    pages,
  };
}

function GetMediaType(file) {
  file = file.toLowerCase();

  if (file.indexOf(".htm") > -1) return "application/xhtml+xml";
  else if (file.indexOf(".jpeg") > -1) return "image/jpeg";
  else if (file.indexOf(".jpg") > -1) return "image/jpeg";
  else if (file.indexOf(".png") > -1) return "image/png";
  else if (file.indexOf(".css") > -1) return "text/css";
  else if (file.indexOf(".ncx") > -1) return "application/x-dtbncx+xml";

  return "application/unknown";
}
