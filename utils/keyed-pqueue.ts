/*jslint node: true, indent:2 */
/*global require*/

/*
    keyed-pqueue.ts
    Copyright (c) 2021 Jocelyn Turpault.
*/
import { debug as d } from "debug";
import { Queue } from "./queue";

const debugPQueue = d("KeyedPQueue");
export class KeyedPQueue {
  private q: Queue;
  private requests: { [key: string]: Promise<any> };
  constructor(options: { concurrency: number; timeout: number }) {
    this.q = new Queue(options.concurrency);
    this.requests = {};
  }
  async add(key: string, fct: () => Promise<any>, options: { atEnd: boolean }): Promise<any> {
    debugPQueue(`Adding request key ${key} (Queue size is ${Object.keys(this.requests).length})`);
    if (Object.keys(this.requests).includes(key)) {
      debugPQueue(`Request ${key} is already pending, returning pending request`);
      return this.requests[key];
    }
    this.requests[key] = this.q.add(fct);
    this.requests[key].finally(() => {
      debugPQueue(`Request ${key} complete`);
      delete this.requests[key];
    });
    return this.requests[key];
  }
}
