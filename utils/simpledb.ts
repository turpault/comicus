import { MD5 } from "crypto-js";
import { debug as d } from "debug";
import { mkdirSync } from "fs";
import { mkdir, readFile } from "fs/promises";
import { join } from "path";
import { clone, values, where } from "underscore";
import { fileExists, filesFromFolderRecursive, safeWriteFile, unlinkNoFail, uuid } from "./fsutils";
import { SimpleDBEntry } from "./types";
import { lock, sleep } from "./utils";
const debug = d("comicus:simpledb");

export function hashToFile(key): { path: string[]; file: string } {
  // hash 3 levels down, to limit the # of files per folder
  const h = MD5(key);
  const strHash = h.words.map((w) => Math.abs(w).toString(36).toUpperCase()).join("");
  const path = [strHash.substr(0, 2), strHash.substr(2, 2)];
  const file = strHash.substr(4);
  return { path, file };
}
const DEFAULT_EVICTION_TIME = 10000; // 10 seconds
export class SimpleDB {
  private path: string;
  private store: any;
  private dirtyIds: { [id: string]: boolean };
  private ttls: { [id: string]: number };
  private mru: number | undefined;
  private evictionTime: number;
  private _closing: boolean;

  constructor(
    path,
    options?: {
      mruSize: number;
      evictionTime: number;
    },
  ) {
    this.mru = options && options.mruSize;
    this.evictionTime = (options && options.evictionTime) || DEFAULT_EVICTION_TIME;
    this.ttls = {};
    this.path = path;
    this.dirtyIds = {};
    this.store = {};
    this._closing = false;
    this._prune();
    mkdirSync(path, { recursive: true });
  }

  async _prune() {
    while (!this._closing) {
      await sleep(2);
      this._pruneNow();
    }
  }

  async close() {
    await this._pruneNow();
    this._closing = true;
  }

  async _pruneNow() {
    const l = await lock("prune");
    const idsToSave = Object.keys(this.dirtyIds);
    if (idsToSave.length > 0) {
      await Promise.all(
        idsToSave.map(async (id) => {
          const { path } = hashToFile(id);
          await mkdir(join(this.path, ...path), { recursive: true });
          const fname = this.filename(id);
          delete this.dirtyIds[id];
          const v = this.store[id];
          debug(`Saving key ${id} to file ${fname}`);
          await safeWriteFile(
            fname,
            JSON.stringify(v, function replacer(key, value) {
              if (value && value.type === "Buffer") {
                return undefined;
              }
              return value;
            }),
          );
        }),
      );
    }
    // Clear the local data - nothing is dirty anymore
    if (Object.keys(this.dirtyIds).length === 0) {
      const now = new Date().getTime();
      // Remove evicted entries
      for (const k of Object.keys(this.store)) {
        if (this.ttls[k] && this.ttls[k] < now) {
          delete this.store[k];
          delete this.ttls[k];
        }
      }
      // Keep only the first mru entries
      if (this.mru) {
        const lst = Object.keys(this.store)
          .map((k) => ({ key: k, val: this.store[k] }))
          .sort((a, b) => {
            if (this.ttls[a.key] && this.ttls[b.key]) {
              return this.ttls[a.key] - this.ttls[b.key];
            }
            if (this.ttls[a.key]) {
              return -1;
            } else if (this.ttls[b.key]) {
              return 1;
            }
            return 0;
          });
        const subset = lst.slice(this.mru);
        for (const l of subset) {
          debugger;
          debug(`Removing Key ${l.key} from memory cache (out of mru list)`);
          delete this.store[l.key];
          delete this.ttls[l.key];
        }
      }
    }
    l();
  }

  add(id, object: SimpleDBEntry): SimpleDBEntry {
    if (!id) {
      id = uuid();
    }
    if (id === "undefined") {
      debugger;
    }
    var o = clone(object);
    o._id = id;
    this.store[id] = o;
    if (this.evictionTime) {
      this.ttls[id] = new Date().getTime() + this.evictionTime;
    }
    this.dirtyIds[o._id] = true;
    return o;
  }

  cached(): SimpleDBEntry[] {
    return Object.values(this.store);
  }

  uncache(id) {
    if (this.dirtyIds[id]) {
      // ignore, it has to be flushed first
    } else {
      debugger;
      delete this.store[id];
    }
  }

  update(object: SimpleDBEntry): SimpleDBEntry {
    if (!object._id) {
      debugger;
      throw new Error("Passed object should have an id");
    }
    this.store[object._id] = object;
    if (this.evictionTime) {
      this.ttls[object._id] = new Date().getTime() + this.evictionTime;
    }

    this.dirtyIds[object._id] = true;
    return object;
  }

  async keys() {
    await mkdir(this.path, { recursive: true });
    const keys: string[] = [];
    const files = await filesFromFolderRecursive(this.path);
    for (const file of files) {
      const filename = join(this.path, file);
      try {
        const e = await fileExists(filename);
        if (e) {
          const b = await readFile(filename, { encoding: "utf-8" });
          const id = JSON.parse(b)._id;
          if (id) {
            keys.push(id);
          }
        }
      } catch (e) {
        unlinkNoFail(filename);
      }
    }
    return keys;
  }

  async delete(o) {
    const _id = o._id || o;
    delete this.store[_id];
    delete this.dirtyIds[_id];
    unlinkNoFail(this.filename(_id));
  }

  filename(id): string {
    const { path, file } = hashToFile(id);
    return join(this.path, ...path, file + ".json");
  }

  async values() {
    const keys = await this.keys();
    const vals = await Promise.all(keys.map((k) => this.get(k)));
    return vals.filter((v) => !!v);
  }

  async merge(id, data) {
    let d = await this.get(id);
    if (!d) {
      return this.add(id, data);
    }
    d = Object.assign(d, data);
    return this.update(d);
  }

  async get(id): Promise<SimpleDBEntry> {
    let res: SimpleDBEntry;
    if (id === undefined) {
      debugger;
    }
    if (this.store[id]) {
      res = this.store[id];
    } else {
      const fname = this.filename(id);
      try {
        if (await fileExists(fname)) {
          const data = await readFile(fname, { encoding: "utf-8" });
          this.store[id] = JSON.parse(data);
          this.store[id]._id = id;
          res = this.store[id];
        }
      } catch (e) {
        // No entry will return undefined
        console.warn("Cannot read file", e.message);
      }
    }
    if (res && this.evictionTime) {
      this.ttls[res._id] = new Date().getTime() + this.evictionTime;
    }
    return res;
  }

  replace(original, newValue) {
    newValue._id = original._id;
    return this.update(newValue);
  }

  async getOrAdd(id, def) {
    const current = await this.get(id);
    if (!current) {
      return this.add(id, def);
    }
    return current;
  }

  where(hash) {
    if (hash.id) return [this.get(hash.id)];
    return where(values(this.store), hash);
  }
}
