#!/usr/bin/env bash  
#clear non unicode chars
IFS="
"
for i in `find "$1" -type d`
  do
    target="${i//[^0-9A-Za-z_. \-\/]/}"
    if [ "$i" != "$target" ]
      then
        mv "$i" "$target"
      fi
  done
for i in `find "$1" -type f`
  do
    target="${i//[^0-9A-Za-z_. \-\/]/}"
    if [ "$i" != "$target" ]
      then
        mv "$i" "$target"
      fi
  done
