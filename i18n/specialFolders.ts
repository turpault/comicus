export const specialFolders = {
  root: "root",
  all: "all",
  recent: "recent",
  progress: "progress",
  progressfolders: "progressfolders",
  favs: "favs",
  surprise_file: "surprise_file",
  dot: ".",
  recentfolders: "recentfolders",
  surprise_folder: "surprise_folder",
};
