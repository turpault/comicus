/*jslint node: true, indent:2 */
/*global require*/

/*
    image.ts
    Copyright (c) 2021 Jocelyn Turpault.
*/
import { MD5 } from "crypto-js";
import { debug as d } from "debug";
import { mkdirSync } from "fs";
import { join } from "path";
import sharp from "sharp";

//import { spawn, Thread, Worker } from "threads";
import { config } from "../../config";
import { isVideo } from "../../utils/bookutils";
import { childProcess, Handler } from "../../utils/worker";
import { fileExists, unlinkNoFail } from "../fsutils";
import { KeyedPQueue } from "../keyed-pqueue";
import { setInterval2 } from "../timerreport";
import { ResizeOptions } from "../types";
import { stat } from "fs/promises";
const debug = d("comicus:image");

const method: string = "sharp";

function mimeFromFormat(format: string): { mime: string; ext: string } {
  var mime = "image/jpeg";
  let ext = ".jpg";
  if (format) {
    switch (format) {
      case "webp":
        ext = ".webp";
        mime = "image/webp";
        break;
      case "png":
        mime = "image/png";
        ext = ".png";
        break;
      default:
        break;
    }
  }
  return { mime, ext };
}

const pool: any[] = [];

const resizeQueue = new KeyedPQueue({ concurrency: 4, timeout: 30000 });

const resizeImageCacheFolder = join(config.cache, "resized-images");
mkdirSync(resizeImageCacheFolder, { recursive: true });
let priority = 0;

export async function resize(fileName: string, options: ResizeOptions): Promise<string | Buffer> {
  if (method === "jimp") return resize_jimp(fileName, options);
  if (method === "sharp") return resize_sharp(fileName, options);
  throw new Error("unknown method");
}

async function resize_sharp(fileName: string, options: ResizeOptions): Promise<Buffer | string> {
  if (isVideo(fileName)) {
    return fileName;
  }
  if (!options.height && !options.width && !options.format) {
    return fileName;
  }
  let s = sharp(fileName, { limitInputPixels: false, failOnError: false }).rotate();
  s = s.resize(
    options.width != undefined ? parseInt(options.width!) : undefined,
    options.height != undefined ? parseInt(options.height!) : undefined,
    { fit: "inside" },
  );
  switch (options.format) {
    case "png":
      s = s.toFormat("png");
      break;
    case "webp":
      s = s.toFormat("webp");
      break;
    case "jpg":
    default:
      s = s.toFormat("jpeg");
  }
  const { data } = await s.toBuffer({ resolveWithObject: true });
  return data;
}
async function resize_jimp(fileName: string, options: ResizeOptions): Promise<string> {
  if (isVideo(fileName)) {
    return fileName;
  }
  const start = new Date().getTime();
  priority++;
  const width =
      options && options.width !== undefined ? parseInt(options.width.toString()) : undefined,
    height =
      options && options.height !== undefined ? parseInt(options.height.toString()) : undefined,
    format = options?.format || "jpg";

  const { ext } = mimeFromFormat(format);
  const imageProcessingLabel = `${options.name} ${width} ${height} ${format}`;

  return resizeQueue.add(
    imageProcessingLabel,
    async () => {
      console.info(`${imageProcessingLabel} Time in queue: ${new Date().getTime() - start} ms`);

      const debugImg = d("image:" + options.name);
      debugImg("Start");
      const resizedFileName =
        join(
          resizeImageCacheFolder,
          `${MD5(fileName).toString()}-${options.width || "auto"}-${options.height || "auto"}`,
        ) + ext;

      debug(`Creating temporary resized image for ${fileName}: ${resizedFileName}`);
      // Remove file if it does not exists
      const c = await fileExists(resizedFileName);
      if (c) {
        const stats = await stat(resizedFileName);
        if (stats && stats.size === 0) {
          await unlinkNoFail(resizedFileName);
        } else if (stats) {
          return resizedFileName;
        }
      }

      let worker: Handler;
      if (pool.length > 0) {
        worker = pool.shift()!;
      } else {
        const w = childProcess(join(__dirname, "./image-worker-jimp"), () => {
          debug("Child process was killed");
        });
        worker = w;
      }
      try {
        await worker.imageResize(fileName, options, resizedFileName);
      } catch (e) {
        unlinkNoFail(resizedFileName);
        throw e;
      } finally {
        pool.push(worker);
      }
      debug(`Temporary resized image for ${fileName} at ${resizedFileName}`);
      return resizedFileName;
    },
    { atEnd: false },
  );
}

setInterval2(async () => {
  // Prune the pool queue from time to time
  if (pool.length > 0) {
    debug("Terminating image resize worker");
    const worker = pool.shift();
    if (worker) {
      worker.terminate();
    }
  }
}, 60000);
