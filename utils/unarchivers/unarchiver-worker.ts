/*
    unarchiver-worker.ts
    Copyright (c)Jocelyn Turpault
*/

import { extname, join } from "path";
import { expose } from "../../utils/worker";
import { filterImages } from "../bookutils";
import { filesFromFolder, filesFromFolderRecursive } from "../fsutils";
import { DecompressedData } from "../types";

import { unzip } from "@jeanarhancet/unzippers";
import { chmod, stat } from "fs/promises";
import { createExtractorFromFile } from "node-unrar-js";
import { mkdir } from "fs/promises";

let debug = console.info;

const supportedTypes: {
  [key: string]: { decompress: Function; extensions: string[]; fallback?: string };
} = {
  "application/x-rar-compressed": {
    decompress: decompressRar,
    extensions: [".cbr", ".rar"],
    fallback: "application/zip",
  },
  "application/zip": {
    decompress: decompressZip,
    extensions: [".zip", ".cbz", ".epub"],
    fallback: "application/x-rar-compressed",
  },
  folder: {
    decompress: decompressFolder,
    extensions: [""],
    fallback: undefined,
  },
};

const inProgress: { [file: string]: any } = {};

/*
setInterval2(function() {
  if(_.keys(inProgress).length > 0) {
    utils.info('Decompress in progress:');
    _.each(inProgress, function(value, key) {
      utils.info(key,':', Math.floor((new Date().getTime() - value)/1000),'s');
    });
  }
}, 10000);*/

async function decompressRar(in_file: string, target: string): Promise<DecompressedData[]> {
  debug("decompressRar");
  await mkdir(target, { recursive: true });
  const extractor = await createExtractorFromFile({ filepath: in_file, targetPath: target });
  const list = extractor.getFileList();
  const fileNames = Array.from(list.fileHeaders);
  const extracted = extractor.extract({ files: fileNames.map((f) => f.name) });
  const files = Array.from(extracted.files);
  await Promise.all(
    files.map(async (f) => {
      await chmod(join(target, f.fileHeader.name), 0o777);
    }),
  );
  return files.map((fileHeader) => {
    return {
      name: fileHeader.fileHeader.name,
      transient: true,
      path: join(target, fileHeader.fileHeader.name),
    };
  });
}

async function decompressZip(file: string, target: string): Promise<DecompressedData[]> {
  debug("decompressZip");
  await unzip(file, { target });
  const decompressedFiles = await filesFromFolderRecursive(target);
  await Promise.all(
    decompressedFiles.map(async (f) => {
      await chmod(join(target, f), 0o777);
    }),
  );
  return decompressedFiles.map((f) => {
    return {
      name: f,
      transient: true,
      path: join(target, f),
    };
  });
}

async function decompressFolder(in_folder: string): Promise<DecompressedData[]> {
  const files = await filesFromFolder(in_folder);
  const pictures = files.filter(filterImages);
  return pictures.map(function (file) {
    var subfile = file.fullname;
    return {
      name: file.name,
      transient: false,
      path: subfile,
    };
  });
}

async function getType(in_file: string): Promise<string> {
  const fileStat = await stat(in_file);

  if (fileStat.isDirectory()) {
    return "folder";
  }
  var ext = extname(in_file).toLowerCase();
  for (const [name, type] of Object.entries(supportedTypes)) {
    if (type.extensions && type.extensions.indexOf(ext) != -1) {
      return name;
    }
  }
  throw new Error("Unknown type");
}

async function decompressFile(file: string, target: string): Promise<DecompressedData[]> {
  debug("Start");
  try {
    inProgress[file] = new Date().getTime();

    const type = await getType(file);
    if (!type) {
      throw new Error("Unknown type");
    }
    if (!supportedTypes[type]) {
      throw new Error("Unsupported type " + type);
    }

    let decompressResult: DecompressedData[] = [];
    try {
      decompressResult = await supportedTypes[type].decompress(file, target);
    } catch (e) {
      // Some extension are lying about their type, try to decompress with the fallback type
      if (supportedTypes[type].fallback !== undefined) {
        decompressResult = await supportedTypes[supportedTypes[type].fallback!].decompress(
          file,
          target,
        );
      } else {
        throw e;
      }
    }
    delete inProgress[file];
    debug("End", file);
    debug("Decompressed", file, "to", target, ":", decompressResult.length, "files");
    return decompressResult;
  } catch (e) {
    delete inProgress[file];
    console.warn(`An error occured while decompressing ${file} to ${target}: ${e}`);
    return [];
  }
}

// If started as a standalone process
if (require.main === module && !process.send) {
  const file = process.argv[2];
  const target = process.argv[3];
  if (!file || !target) {
    console.log("Usage: unarchiver-worker <file> <target>");
    process.exit(1);
  }
  decompressFile(file, target).then((res) => {
    console.log(res);
    process.exit(0);
  });
} else {
  expose({
    decompress: decompressFile,
  });
}
