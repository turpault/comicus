import { debug as d } from "debug";
import { keys } from "underscore";
const _setInterval = global.setInterval;
const _setTimeout = global.setTimeout;
const _clearInterval = global.clearInterval;
const _clearTimeout = global.clearTimeout;

const debug = d("comicus:timerreport");

const intervals = {};
const timeouts = {};

export const setInterval2 = function (fct, ...args) {
  const e = new Error();
  const res = _setInterval.call(
    this,
    async () => {
      try {
        await fct();
      } catch (ex) {
        debug("Exception in setInterval - original location is ", e, ex);
      }
    },
    ...args,
  );
  intervals[res] = new Error().stack;
  return res;
};

export const clearInterval2 = function (intervalId) {
  const res = _clearInterval.apply(this, arguments);
  delete intervals[intervalId];
  if (keys(intervals).length > 0) {
    console.info("Cleared interval, ", keys(intervals).length, "left");
  }
  return res;
};

let timeoutId = 0;
export const setTimeout2 = function (fct, ...args) {
  const e = new Error();
  const cb = args[0];
  let res = 0;
  const id = timeoutId++;
  const tap = function () {
    delete timeouts[id];
    if (keys(timeouts).length > 0) {
      debug("[Timers] Called timeout, ", keys(timeouts).length, "left");
    }
    cb.call(this, ...args);
  };
  args[0] = tap;
  res = _setTimeout.call(
    this,
    async () => {
      async () => {
        try {
          await fct();
        } catch (ex) {
          debug("Exception in setTimeout cb - original location is ", e, ex);
        }
      };
    },
    ...args,
  );
  (res as any).timeoutId = id;
  timeouts[id] = new Error().stack;
  return res;
};

export const clearTimeout2 = function (timeoutId) {
  const res = _clearTimeout(timeoutId);
  const id = (timeoutId as any).timeoutId;
  if (id) delete timeouts[id];
  if (keys(timeouts).length > 0) {
    console.info("Cleared timeout, ", keys(timeouts).length, "left");
  }
  return res;
};

export const listTimers = function () {
  return {
    intervals: intervals,
    timeouts: timeouts,
  };
};
