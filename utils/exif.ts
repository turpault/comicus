import { read } from "fast-exif";
export async function exifFromFile(fileName: string): Promise<any> {
  const exifData = await read(fileName);
  console.info("exif", fileName, exifData);
  return exifData;
}
