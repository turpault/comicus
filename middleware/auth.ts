/*
    auth.js
    Copyright (c) Jocelyn Turpault
*/
/*
    Implements the basic authentication mechanisms
*/

import basicAuth from "basic-auth";
import crypto from "crypto-js";
import d from "debug";
import { t } from "../i18n/translation";
import { config } from "../config";
import { clearInterval2, setInterval2 } from "../utils/timerreport";
const debug = d("comicus:auth");

function unauthorized(res) {
  if (res) {
    res.set("WWW-Authenticate", "Basic realm=Authorization Required");
    res.sendStatus(401);
  }
  return false;
}

export function _encodeContentKey(seed) {
  return seed + "-" + crypto.HmacSHA256(seed, "MySecretIsBetter").toString(crypto.enc.Hex);
}
var _validate = function (key) {
  var s = key.split("-"),
    v = s.pop(),
    seed = s.join("-");
  return seed && v && key === _encodeContentKey(seed);
};

export const contentkeyauth = function (req, res, next) {
  if (!config.auth) {
    return next();
  }

  var key = req.query.contentkey;
  if (key && key.length && !key.substr) {
    key = key[0];
  }
  if (!key) {
    debug("No security key");
    if (!config.options_debug) {
      return unauthorized(res);
    }
  } else {
    if (!_validate(key)) {
      debug("### Invalid key", key);
      if (!config.options_debug) {
        return unauthorized(res);
      }
    }
  }
  return next();
};

export const basicauth = function (req, res, next?) {
  if (!config.auth) {
    if (next) {
      return next();
    } else {
      return true;
    }
  }

  var user = basicAuth(req);
  if (!user) {
    return unauthorized(res);
  }
  // validate against admin user and password
  var valid = user.name === "admin" && user.pass === config.admin_password;

  if (!valid) {
    return unauthorized(res);
  }

  req.admin = true;
  if (next) return next();
  return true;
};

export const basicorsecretauth = function (req, res, next) {
  if (!config.auth) {
    return next();
  }

  if (!basicauth(req, null, null) && !secretAuthAccess(req, null, null)) {
    return unauthorized(res);
  }
  next();
};

var recentconces = {};
// scrub old nonces evey 5 minutes, remove anything older than one hour
var conceCleanupInterval = setInterval2(
  function () {
    // scan recent conces, and remove old ones
    var limit = new Date().getTime() / 1000 - 3600;
    var removed = 0;
    for (var i in recentconces) {
      if (recentconces[i].expire < limit) {
        removed++;
        delete recentconces[i];
      }
    }
    if (removed) debug("Removed old conces", removed, " conces removed");
  },
  1000 * 60 * 5,
);

export const secretAuth = function (pin) {
  return function (req, res, next) {
    if (!config.auth) {
      return next();
    }

    var conce = req.headers.conce,
      sign = req.headers.signature,
      password = pin;

    if (!sign || !conce || !password) {
      debug("Missing ", sign ? "" : "Signature", conce ? "" : "Conce");
      if (!config.options_debug) {
        if (res) {
          throw new Error("Invalid User or Password");
        }
        return false;
      }
    } else {
      var hash = crypto.HmacSHA256(conce || "", password || "").toString(crypto.enc.Hex);

      if (hash != sign) {
        debug("Hash invalid", hash, "vs", sign);
        if (!config.options_debug) {
          debug("req.app.result", req.app.result);
          if (res) {
            throw new Error("Invalid Password");
          }
          return false;
        }
      }
      var concekey = conce;
      if (recentconces[concekey] && req.url !== recentconces[concekey].request) {
        debug(
          "Tried to replay with conce ",
          concekey,
          "replay url is ",
          req.url,
          " original url was",
          recentconces[concekey].request,
        );
        if (!config.options_debug) {
          if (res) {
            throw new Error("Stop doing replay attacks please");
          }
          return false;
        }
      }
      // Check expired conce
      var conceTimestamp = new Date(parseInt(conce.split("-")[0], 36));
      if (Math.abs(conceTimestamp.getTime() - new Date().getTime()) > 10000) {
        // 10 seconds of drift - bad
        debug("Expired conce");
        if (false) {
          if (res) {
            return req.app.result(
              req,
              res,
              t("Clock drifting - please check your local time. Server time is :", req.query.lang) +
                new Date().toString() +
                t(", Received time is :", req.query.lang) +
                conceTimestamp.toString(),
            );
          }
          return false;
        }
      }
      recentconces[concekey] = {
        expire: Math.floor(new Date().getTime() / 1000),
        request: req.url,
      };
    }

    next && next();
    return true;
  };
};

export const secretAuthAccess = secretAuth(config.pin_access);
export const secretAuthAdmin = secretAuth(config.pin_admin);

export const cookieKey = "@sjfoiWER1";
// Authentication is required
export const install = function (app) {
  app.get("/validatePin", function (req, res) {
    var type = req.query.type,
      conce = req.query.pinconce,
      pin = req.query.pin;

    var realpin = config[type];
    debug(realpin);
    if (realpin === "") {
      return app.result(req, res);
    }
    var cryptedReal = crypto.HmacSHA256(conce + realpin, "secret").toString(crypto.enc.Hex);
    if (cryptedReal === pin) {
      return app.result(req, res);
    }
    debug("Pin provided for type", type, "is invalid");
    throw new Error("invalid pin");
  });
};

export const close = function (cb) {
  clearInterval2(conceCleanupInterval);
  cb();
};
