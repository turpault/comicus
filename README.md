Comic Reader based on NodeJS, Enyo (for the client app)
=======================================================

This project was born when I could not find any useful tool that would allow me to read my comic collection on my iPad or my PC.

Since then, this has become like a pet project, and I continue adding features to it.

There are a lot of apps doing that task, but Apple seems to be very reluctant to have them download digital content. Moreover, handling eveything at the client side seemed to be a bit overkill.

Therefore, as I also wanted to learn how nodejs worked, and other technologies along the way, I've decided to start working on my own project.

How it works
------------

The NodeJS Server process is basically a web api that lists files, open the contents of .cbr. cbz .zip and .rar files, extract the images, and provide an image list to a client app.

The server extracts the pictures in the /tmp folder, and can resize them on-the-fly (because my iPad has a maximum image size).

Amoung other perks:
* Conversion to epub format (to be downloaded and read as a book in your favorite reader).
* 'Recently added file' subselection (useful to find newly added books).

Requirements
------------

Server-side:
* ImageMagick
* rar/unrar
* zip/unzip
* nodejs

Client-size:
* A browser with javascript support (Tested on Opera, Chrome, Safari Desktop, Safari Mobile).

Also includes the cordova project, allowing the hybrid app to be build (certs are not provided, though).

Setup
-----

> npm install
> npm run deploy
> node main.js

The default folder served is the content folder. In order to use https, a https certificate and its key must be installed in the certs-https folder.

Then open
http://127.0.0.1:8888/app