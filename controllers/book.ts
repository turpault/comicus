import { debug as d } from "debug";
import getExpeditiousCache from "express-expeditious";
import { createWriteStream, existsSync } from "fs";
import { mkdir, readFile } from "fs/promises";
import * as ini from "js-ini";
import { basename, dirname, isAbsolute, join, sep } from "path";
import { clone, compact, difference, omit, pluck, sample, uniq, where } from "underscore";
import { inspect } from "util";
import { config } from "../config";
import { specialFolders } from "../i18n/specialFolders";
import { t } from "../i18n/translation";
import { contentkeyauth } from "../middleware/auth";
import {
  filterBooks,
  filterImages,
  hasExif,
  pageSortAndFilter,
  sanitizeTitle,
  sortItemArrayByDateFct,
  sortItemArrayByNameFct,
  sortItemByDateInNameFct,
} from "../utils/bookutils";
import { convert } from "../utils/converters/convert";
import { exifFromFile } from "../utils/exif";
import { ExpressCache } from "../utils/express-cache";
import { allStats, fileExists, memoizer, uuid } from "../utils/fsutils";
import { getArchiveFiles } from "../utils/mrf";
import { Queue } from "../utils/queue";
import { SimpleDB } from "../utils/simpledb";
import * as streamers from "../utils/streamers";
import { resizeAndStream, streamFile } from "../utils/streamers";
import { DecompressedData, Item, PagesWithExif } from "../utils/types";
import { lock, sleep } from "../utils/utils";
import { filter, items, merge } from "./persona";
const debug = d("comicus:book");

var exclude, thumbFolder;

let alldata: Item[] = [];

let bookDB: SimpleDB;
let allPictureData: SimpleDB;
let recentdata, recentdatafolders;

enum Keys {
  recentdataKey = "recentdata",
  recentdatafoldersKey = "recentdatafolders",
  alldataKey = "alldata",
}

function itemIdFromPath(path) {
  return sanitizeTitle(path)
    .replace(sep, "-")
    .replace(/[^a-zA-Z0-9]/gim, "-")
    .replace(/--+/g, "-");
}

function itemIsFile(item) {
  return item.type === "file";
}

export function itemFromId(itemid: string, lang?: string, personaid?: string): Item {
  const e = getUserFolder(itemid);
  if (e && e.user) {
    personaid = e.user;
    itemid = e.folder;
  }

  // Fake items, always available
  if (Object.values(specialFolders).includes(itemid)) {
    return {
      text: lang ? t(itemid, lang) : itemid,
      uid: userFolder(itemid, personaid),
      candownload: false,
      parentuid: "root",
      path: ".",
      type: "folder",
      kid: true,
      adult: false,
      ctime: new Date(),
      mtime: new Date(),
    };
  }

  if (!alldata) {
    throw new Error("Scan not done yet");
  }
  const found = alldata.find((d) => d.uid == itemid);
  return found!;
}

async function directoryIsAlbumOrFolder(fullpath): Promise<"file" | "folder"> {
  let files = await allStats(fullpath);
  files = files
    .filter(function (file) {
      return !file.name.startsWith(".") && !exclude.includes(file.name.toLowerCase());
    })
    .filter((file) => !file.name.startsWith("."));
  for (const stat of files) {
    if (stat.isDirectory()) {
      return "folder";
      break;
    } else if (filterImages(stat)) {
      return "file";
      break;
    } else if (filterBooks(stat)) {
      return "folder";
      break;
    }
  }
  return "folder";
}

async function scanPictureData(pathName) {
  const queue = new Queue(10);
  const pictureData = {};
  async function scanFolder(folder) {
    var folderpath = join(config.root, folder);
    const allstats = await allStats(folderpath);
    const images = allstats.filter(filterImages);
    images.forEach((image) =>
      queue
        .add(async () => {
          let existingData = await allPictureData.getOrAdd(image.fullname, image);
          if (!existingData.exif && hasExif(image)) {
            await exifFromFile(image.fullname)
              .then(async (e) => {
                await allPictureData.merge(image.fullname, { exif: e });
              })
              .catch((e) => {
                debug("An error occured while getting exif data for", image.fullname, e);
              });
          }
        })
        .catch((e) => {
          console.error(`Error while checking ${image.name}`, e);
        }),
    );
    // Get picasa files in there
    const picasafiles = allstats.filter((file) =>
      [".picasa.ini", "Picasa.ini"].includes(file.name),
    );
    for (const f of picasafiles) {
      const fName = join(config.root, folder, f.name);
      const iniData = await readFile(fName, "utf-8");
      try {
        const iniFile = ini.parse(iniData);
        Object.keys(iniFile).forEach((iniFileEntry) => {
          if (!["Contacts", "Contacts2"].includes(iniFileEntry)) {
            const entry = iniFile[iniFileEntry] as { star: string; faces: string };
            const p = join(config.root, folder, iniFileEntry);
            if (entry.faces || entry.star) {
              queue
                .add(async () => {
                  let existingData = await allPictureData.get(p);
                  if (existingData && !existingData.meta) {
                    await allPictureData.merge(p, {
                      meta: {
                        star: entry.star,
                        faces: entry.faces,
                      },
                    });
                  }
                })
                .catch((e) => {
                  console.error(`Error while faces in: ${iniFile}`, e);
                });
            }
          }
        });
      } catch (e) {
        /* ignore */
        debugger;
      }
    }

    for (const d of allstats.filter(
      (s) => s.isDirectory() && !s.name.startsWith(".") && !exclude.includes(s.name.toLowerCase()),
    )) {
      queue
        .add(() => scanFolder(join(folder, d.name)))
        .catch((e) => {
          console.error(`Error while scanning ${folder}/${d.name}`, e);
        });
    }
  }

  queue
    .add(() => scanFolder(pathName))
    .catch((e) => {
      console.error(`Error while scanning ${pathName}`, e);
    });

  await queue.drain();

  return pictureData;
}

// Scanning for recent files
async function scanFiles(pathname) {
  var filesMatching = [];
  const queue = new Queue(10);
  const interval = setInterval(() => {
    debug(
      `ScanFiles Queue : ${queue.done()} / ${queue.total()}, found ${filesMatching.length} files`,
    );
  }, 2000);

  const walk = async (p: Item) => {
    debug(`Processing: ${p.path}`);
    const interval2 = setInterval(() => {
      debug(`Still processing : ${p.path}`);
    }, 2000);
    try {
      const items = await albumsAndFoldersInFolder(p);
      debug(`Processing: ${p.path} : got ${items.length} items`);
      items.forEach(function (item) {
        if (item.type === "folder") {
          queue
            .add(async () => walk(item))
            .catch((e) => {
              console.error(e);
            });
        }
      });
      [].push.apply(filesMatching, items);
    } finally {
      clearInterval(interval2);
    }
  };
  queue
    .add(
      async () =>
        await walk({
          uid: specialFolders.dot,
          parentuid: specialFolders.dot,
          path: pathname,
          text: "",
          type: "folder",
          ctime: new Date(),
          mtime: new Date(),
          kid: false,
          adult: false,
        }),
    )
    .catch((e) => {
      console.error(`Error while walking ${pathname}`, e);
    });
  await queue.drain();
  clearInterval(interval);
  return filesMatching;
}

export async function rescanFiles() {
  const l = await lock("rescanFiles");
  try {
    debug("Starting scan");
    var now = new Date().getTime();
    var initial = alldata.length === 0;
    const dataPromise = scanFiles(".");

    const data = await dataPromise;

    debug("Retrieved", data.length, "items during scan");

    var olddatauids: string[] = [];
    if (alldata) {
      olddatauids = alldata.filter(itemIsFile).map(function (t) {
        return t.uid;
      });
    }
    alldata = bookDB.add(Keys.alldataKey, data);

    // post-process for the kid flag
    alldata.forEach(function (book) {
      if (book.kid) {
        //utils.log('Book',book.text,'is kid enabled');
        var f = function (book) {
          if (book.parentuid === specialFolders.dot) return;
          // file deepcode ignore PT: <please specify a reason of ignoring this>
          var p = itemFromId(book.parentuid);
          if (p) {
            p.kid = true;
            f(p);
          }
        };
        f(book);
      }
    });

    var filesbydate = alldata.filter(itemIsFile).sort(sortItemArrayByDateFct);
    recentdata = bookDB.add(Keys.recentdataKey, filesbydate.slice(0, 100));
    var parents = filesbydate
      .filter((f) => f.parentuid)
      .map(function (file) {
        var parent = itemFromId(file.parentuid);
        // parent.modifiedChild = file;
        return parent;
      });

    var uniqueParents = uniq(parents);
    recentdatafolders = bookDB.add(Keys.recentdatafoldersKey, uniqueParents.slice(0, 100));

    if (initial) {
      debug("Initial scan complete");
    } else {
      var datauids = alldata.filter(itemIsFile).map(function (t) {
        return t.uid;
      });
      var newdatauids = difference(datauids, olddatauids);
      var newFiles = newdatauids.map((id) => itemFromId(id));
      debug("New files : ", newFiles.length, "found");
      if (newFiles.length > 0) {
        var titles = newFiles.map(function (t) {
          return t.text;
        });
        var message = "Nouveaux albums :" + titles.join(",");
        if (message.length > 100) message = message.substr(0, 100) + "...";
        //notification.push(message);
      }
    }

    var then = new Date().getTime();

    console.info("Scan completed in " + (then - now) / 1000 + " seconds");
    const q = new Queue(5);
    data.forEach((item) => {
      if (itemIsFile(item)) {
        q.add(() =>
          thumbpathForAlbum(item).catch((e) =>
            debug("An error occured while creating the thumbnail", e),
          ),
        );
      }
    });

    await q.drain();
    console.info("Thumbnails created in " + (new Date().getTime() - then) / 1000 + " seconds");
  } catch (e) {
    debug("An error occured while scanning:", e);
  }
  l();
}

export function sortItems(items: Item[], sort?: string): Item[] {
  if (!sort) {
    sort = "";
  }
  switch (sort.split(/[+-]/)[0]) {
    case "none":
      break;
    case "date":
      items = items.sort(sortItemByDateInNameFct);
      break;
    case "cdate":
      items = items.sort(sortItemArrayByDateFct);
      break;
    case "name":
    default:
      items = items.sort(sortItemArrayByNameFct);
      break;
  }

  if (sort.slice(-1) === "-") {
    items = items.reverse();
  }
  return items;
}

export async function populatePageData(pages: DecompressedData[]): Promise<PagesWithExif[]> {
  const pageWithExif: PagesWithExif[] = await Promise.all(
    pages.map(async (page) => {
      if (page.path) {
        const withExif = await allPictureData.get(page.path);
        if (withExif) {
          if (!withExif.name) debugger;
          return withExif;
        }
      }
      return { ...page, star: false, exif: { exif: {} } };
    }),
  );

  return pageWithExif;
}

export async function pages(albumid, persona, options) {
  const sort = options.sort;
  const item = itemFromId(albumid);
  if (!item) {
    throw new Error("No such album");
  }
  var albumpath = item.path;
  let pagespaths = await pagesFromPath(albumpath);
  // Get exif data from these files
  let withExif = await populatePageData(pagespaths);
  if (sort) {
    switch (sort.split(/[+-]/)[0]) {
      case "date":
        {
          const sorted = withExif.sort((a: PagesWithExif, b: PagesWithExif) => {
            try {
              return a.exif.exif.DateTimeOriginal < b.exif.exif.DateTimeOriginal
                ? -1
                : a.exif.exif.DateTimeOriginal > b.exif.exif.DateTimeOriginal
                  ? 1
                  : 0;
            } catch (e) {
              return -1;
            }
          });
          if (sort.slice(-1) === "-") {
            pagespaths = sorted.reverse();
          } else {
            pagespaths = sorted;
          }
        }
        break;
      case "name":
        // Already sorted
        if (sort.slice(-1) === "-") {
          withExif = withExif.reverse();
        }
        break;
    }
  }
  var urls = withExif.map(function (page) {
    return {
      url: "/pages/" + albumid + "/page/" + encodeURIComponent(page.name),
      path: join(albumpath, page.name),
      fav: page.star && page.star !== undefined,
      exif: page.exif,
    };
  });
  return urls;
}

const jobs = {};

export async function createJob(albumid, format, personaid, lang): Promise<string> {
  const newItem = clone(itemFromId(albumid));

  const jobId = uuid();
  jobs[jobId] = newItem;

  newItem.inprogress = true;
  try {
    const out = await getBook(newItem, format, personaid, lang);

    newItem.inprogress = false;
    newItem.out = out;
  } catch (err) {
    newItem.status = err;
  }

  return jobId;
}

export function getJobs(lang) {
  return jobs;
}

export function getJob(jobid, lang?) {
  if (!jobs[jobid]) throw new Error("No such job");
  return omit(jobs[jobid], "out");
}

export function deleteJob(jobId) {
  if (jobs[jobId]) {
    delete jobs[jobId];
  }
}

export async function streamJob(jobId, options, pipe) {
  const streamer = options.streamer;
  const job = getJob(jobId);
  if (job.status) {
    return job.status;
  }
  if (!job.out) {
    throw new Error("No stream");
  }

  var streamerFct = streamers[streamer] || streamers.raw;
  return streamerFct(job.out.stream, job.out.title, job.out.mime, pipe);
}

export async function getBook(
  item: Item,
  format: "pdf" | "epub" | "cbz",
  personaid: string,
  options,
): Promise<{ title: string; file: string; mime: string }> {
  var mime = {
    pdf: "application/pdf",
    epub: "application/epub+zip",
    cbz: "application/cbz",
  }[format];
  debug("Getting book ", item.uid, "using format", format);

  // Get all the pages for that book or folder
  const items: Item[] = [];
  const addItem = async (item) => {
    if (itemIsFile(item)) {
      items.push(item);
    } else {
      const lst = await folder(item.uid, personaid, options);
      for (const i of lst) {
        await addItem(i);
      }
    }
  };
  await addItem(item);

  const allPageForAllBooks = await Promise.all(items.map((item) => pagesFromPath(item.path)));
  const allPages = allPageForAllBooks.reduce((prev, cur) => [...prev, ...cur], []);
  const file = await convert(item, format, allPages);
  return { title: item.text + "." + format, file, mime };
}

/*export async function exif(imagePath) {
  if (!isAbsolute(imagePath)) {
    imagePath = join(config.root, imagePath);
  }
  if (!hasExif(imagePath)) {
    return {};
  }
  return new Promise(
    (resolve, reject) =>
      new ExifImage({ image: imagePath }, function (error, exifData) {
        if (error) {
          if (error.code === "NO_EXIF_SEGMENT") {
            resolve({});
          } else {
            reject(error);
          }
          return;
        } else resolve(exifData); // Do something with your data!
      })
  );
}*/
const pageMemoizer = memoizer(100);
export async function pagesFromPath(albumpath: string): Promise<DecompressedData[]> {
  // Opens an Archive, and returns a list of links
  var file = join(config.root, albumpath);

  const start = new Date().getTime();
  debug("Start getting pages from ", albumpath);

  const files = await pageMemoizer(["getArchiveFiles", file], () =>
    getArchiveFiles(file, pageSortAndFilter),
  );
  if (!files) {
    debug("No pages retrieved for", file);
  } else {
    debug(
      "Pages retrieved for ",
      albumpath,
      ":",
      files.length,
      "pages in",
      (new Date().getTime() - start) / 1000,
      "s",
    );
  }
  return files;
}

export async function recent(personaid) {
  const data = await merge(personaid, recentdata);
  return filter(personaid, data);
}

export async function recentfolders(personaid) {
  const data = await merge(personaid, recentdatafolders);
  return filter(personaid, data);
}

export async function surprise(personaid: string, type: "file" | "folder", count: number) {
  var items = alldata;
  if (type) items = where(items, { type: type });
  const filtered = await filter(personaid, items);
  const sampled = sample(filtered, count);
  return merge(personaid, sampled);
}

export async function albumsAndFoldersInFolder(parent): Promise<Item[]> {
  if (parent.path === "ToBeSorted") {
    await sleep(5);
    debugger;
  }
  var folderpath = join(config.root, parent.path);
  const allstats = await allStats(folderpath);
  const items: Item[] = [];
  for (const stat of allstats) {
    if (stat.name.startsWith(".") || exclude.includes(stat.name.toLowerCase())) {
      continue;
    }
    const relPath = join(parent.path, stat.name);
    if (stat.isSymbolicLink()) {
      continue;
    } else if (stat.isDirectory()) {
      const dotFiles = await allStats(join(config.root, relPath));
      const dotFileNames = dotFiles.filter((s) => s.name.startsWith(".")).map((s) => s.name);
      const kid = dotFileNames.includes(".kid");
      const adult = dotFileNames.includes(".adult");
      const child = stat.fullname;
      const type = await directoryIsAlbumOrFolder(child);
      if (type) {
        items.push({
          text: sanitizeTitle(stat.name, true),
          type,
          parentuid: parent.uid,
          uid: itemIdFromPath(relPath),
          ctime: stat.ctime,
          mtime: stat.mtime,
          path: relPath,
          kid: parent.kid || kid || false,
          adult: parent.adult || adult || false,
        });
      }
    } else if (filterBooks(stat.name)) {
      items.push({
        text: sanitizeTitle(stat.name, true),
        type: "file",
        parentuid: parent.uid,
        uid: itemIdFromPath(relPath),
        ctime: stat.ctime,
        mtime: stat.mtime,
        path: relPath,
        kid: parent.kid || false,
        adult: parent.adult || false,
      });
    }
  }
  return items;
}

export async function search(
  q: string | string[],
  personaid: string,
  lang: string,
  sort?: string,
): Promise<Item[]> {
  if (!Array.isArray(q)) {
    q = [q];
  }
  if (!alldata) {
    throw new Error("Scan in progress");
  }
  const terms = q.map((v) => v.toUpperCase());

  var items = alldata
    .filter(function (item) {
      return terms.filter((v) => item.text.toUpperCase().includes(v)).length === q.length;
    })
    .slice(0, 100);
  if (!personaid) {
    return items;
  }
  items = await merge(personaid, items);
  items = await filter(personaid, items);
  items = sortItems(items, sort);
  return items;
}

export async function folder(
  folderid: string | undefined,
  personaid: string,
  options,
): Promise<Item[]> {
  const lang = options.lang;
  let sort = options.sort;
  let data: undefined | Item[];
  if (!alldata) {
    throw new Error("Initial scan not completed");
  }

  // The user folder id might contain a persona
  const e = getUserFolder(folderid);
  if (e && e.user) {
    personaid = e.user;
    folderid = e.folder;
  }

  if (folderid === specialFolders.root) {
    if (config.clientMode === "bd") {
      data = [
        specialFolders.dot,
        specialFolders.recentfolders,
        specialFolders.progressfolders,
        specialFolders.favs,
        specialFolders.surprise_folder,
      ].map((id) => itemFromId(id, lang, personaid));
    } else {
      data = [
        specialFolders.all,
        specialFolders.recent,
        specialFolders.progress,
        specialFolders.favs,
        specialFolders.surprise_file,
      ].map((id) => itemFromId(id, lang, personaid));
    }
  }
  if (folderid === specialFolders.all) {
    data = alldata.filter((item) => item.type === "file");
  }
  if (folderid === specialFolders.recent) {
    sort = "none"; // already sorted
    data = await recent(personaid);
  }
  if (folderid === specialFolders.recentfolders) {
    sort = "none"; // already sorted
    data = await recentfolders(personaid);
  }
  if (folderid === specialFolders.progress) {
    sort = "none";
    data = await items(
      personaid,
      function (item) {
        return item.progress === true || (item.page > 0 && item.read == false);
      },
      "lastChanged-",
    );
    data = compact(data.map((f) => itemFromId(f.uid, lang, personaid)));
  }
  if (folderid === specialFolders.progressfolders) {
    sort = "none";
    const progressFolders = await items(
      personaid,
      (item) => {
        return item.progress === true || (item.page > 0 && item.read == false);
      },
      "lastChanged-",
    );
    data = compact(
      uniq(
        pluck(compact(progressFolders.map((f) => itemFromId(f.uid, lang, personaid))), "parentuid"),
      ).map((id) => where(alldata, { uid: id })[0]),
    );
  }
  if (folderid === specialFolders.favs) {
    sort = "name";
    data = await items(
      personaid,
      function (item) {
        return item.favorite;
      },
      "text",
    );
    data = compact(data.map((f) => itemFromId(f.uid, lang, personaid)));
  }
  if (folderid === specialFolders.surprise_file) {
    data = await surprise(personaid, "file", 50);
  }
  if (folderid === specialFolders.surprise_folder) {
    data = await surprise(personaid, "folder", 50);
  }
  debug("Requested folder:", folderid);

  if (alldata !== undefined && data === undefined) {
    folderid = folderid || "";

    data = alldata.filter(function (item) {
      return item.parentuid == folderid;
    });
  }
  data = await merge(personaid, data!);

  data = await filter(personaid, data);
  data = sortItems(data!, sort);

  return data!;
}

function userFolder(name: string, user?: string) {
  if (user) return name + "~" + user;
  return name;
}

function getUserFolder(id) {
  if (id.includes("~")) {
    const [folder, user] = id.split("~");
    return { folder, user };
  }
  return undefined;
}

export async function page(albumid, pageid, options, stream) {
  debug(`page(${albumid}, ${pageid}, ${options})`);
  var albumpath = itemFromId(albumid).path;
  const pagespaths = await pagesFromPath(albumpath);
  var pagename = decodeURIComponent(pageid);
  var page = where(pagespaths, { name: pagename });
  if (!page || page.length === 0) {
    debug("Page not found", pageid, pagename);
    throw new Error("Page not found");
  }
  options.name = page[0].name;
  if (!page[0].path) {
    debug(`No path for page ${pageid} in album ${albumid}`);
    debugger;
  }
  resizeAndStream(page[0].path, options, stream);
}

async function thumb(id, personaid, options, stream) {
  const lang = options.lang;
  if (!personaid) {
    const e = getUserFolder(id);

    if (e && e.user) {
      personaid = e.user;
      id = e.folder;
    }
  }
  const item = itemFromId(id);
  if (
    [
      "recent",
      "recentfolders",
      "progress",
      "progressfolders",
      "favs",
      "surprise_file",
      "surprise_folder",
    ].includes(id) ||
    (item && item.type === "folder")
  ) {
    stream.set("cache-control", "No-Cache");
    const items = await folder(id, personaid, lang);
    const selectedItem = sample(items);
    if (selectedItem) {
      return thumb(selectedItem.uid, personaid, options, stream);
    }
    options.name = "nothumb.jpg";
    return streamFile(join("resources", "nothumb.jpg"), options, stream);
  }
  if (!item) {
    stream.set("cache-control", "No-Cache");
    options.name = "nothumb.jpg";
    return streamFile(join("resources", "nothumb.jpg"), options, stream);
  }

  const thumbpath = await thumbpathForAlbum(item);
  console.assert(isAbsolute(thumbpath));
  options.name = thumbpath;
  return streamFile(thumbpath, options, stream);
}

export async function thumbpathForAlbum(album: Item): Promise<string> {
  console.assert(itemIsFile(album));
  // Check if the file already exists
  var file = basename(dirname(album.path)) + basename(album.path);
  var absfile = join(thumbFolder, file + ".jpg");

  if (await fileExists(absfile)) {
    return absfile;
  } else {
    debug("Generating thumbnail for", album.text);
    let source: string;
    let filename: string;
    try {
      const data = await pagesFromPath(album.path);
      if (data === undefined || data.length < 1) {
        throw new Error("No picture");
      }
      debug("Pages for", album.path, ":", data.length, "pages");
      var sorted = pageSortAndFilter(data);
      // scale it down
      source = sorted[0].path;
      filename = sorted[0].name;
    } catch (e) {
      console.error(`Error while getting pages for ${album.path}: ${e}, ${e.stack}`);
      source = join("resources", "nothumb.jpg");
      filename = album.text;
    }
    try {
      await resizeAndStream(
        source,
        { name: album.text, width: "250", format: "jpg" },
        createWriteStream(absfile),
      );
    } catch (e) {
      console.error(
        `Error while resizing image for ${album.path}, from ${source}: ${inspect(e)} /  ${
          e.message
        }, ${e.stack}, picture is ${source}`,
      );
      source = join("resources", "nothumb.jpg");
      await resizeAndStream(
        source,
        { name: album.text, width: "250", format: "jpg" },
        createWriteStream(absfile),
      );
    }
    debug("Generated thumbnail for", album.text, "using file", filename, "saved as", absfile);
    return absfile;
  }
}

export function exists(album: Item) {
  if (album.path) {
    var albumpath = album.path;
    var file = join(config.root, albumpath);
    var exists = existsSync(file);
    var isbook = filterBooks(file);
    return exists && isbook;
  } else {
    // Poor-man implementation - validate against alldata
    return itemFromId(album.uid) !== undefined;
  }
}

let fsCache: ExpressCache;
export async function install(app) {
  exclude = config.exclude.split(",").map(function (v) {
    return v.toLowerCase();
  });
  thumbFolder = join(config.cache, "thumbnails");
  if (!isAbsolute(thumbFolder)) {
    throw new Error("Thumbnail folder must be absolute");
  }

  await mkdir(thumbFolder, { recursive: true });

  bookDB = new SimpleDB(join(config.db, "books"));
  allPictureData = new SimpleDB(join(config.db, "pictureData"));

  recentdata = await bookDB.getOrAdd(Keys.recentdataKey, []);
  recentdatafolders = await bookDB.getOrAdd(Keys.recentdatafoldersKey, []);
  alldata = await bookDB.getOrAdd(Keys.alldataKey, []);

  fsCache = new ExpressCache();
  const cache = getExpeditiousCache({
    // Namespace used to prevent cache conflicts, must be alphanumeric
    namespace: "expresscache",

    engine: fsCache,
    defaultTtl: "1 hour",
    statusCodeExpires: { "304": 0 },
  });

  app.use((err, req, res, next) => {
    debugger;
    res.status(500);
    res.render("error", { error: err });
  });

  app.result = async (req, res, f, status) => {
    let data = f;
    if (f instanceof Promise) {
      data = await f;
    }
    res.status(status || 200);
    res.json({ error: null, data });
  };
  app.resultRaw = async (req, res, f, status) => {
    let data = f;
    if (f instanceof Promise) {
      data = await f;
    }
    res.status(status || 200);
    res.send(data);
  };

  app.getAsync("/pages/:albumid/page/:pageid", contentkeyauth, async (req, res) => {
    await page(req.params.albumid, req.params.pageid, req.options, res);
  });

  app.getAsync("/thumbnail/:albumid", contentkeyauth, async (req, res) => {
    await thumb(req.params.albumid, req.personaid, req.options, res);
  });
  app.getAsync("/thumbnail", contentkeyauth, async (req, res) => {
    await thumb("", req.personaid, req.options, res);
  });

  app.getAsync("/pages/:albumid", contentkeyauth, cache, async (req, res) => {
    const p = await pages(req.params.albumid, req.personaid, req.options);
    const urls = p.map((page) => page.url);
    await app.result(req, res, urls);
  });

  app.getAsync("/pdf/:albumid", contentkeyauth, async (req, res) => {
    const item = itemFromId(req.params.albumid);
    if (!item) {
      throw new Error("No such album");
    }
    const out = await getBook(item, "pdf", req.personaid, req.options);
    res.download(out.file, out.title);
  });
  app.getAsync("/cbz/:albumid", contentkeyauth, async (req, res) => {
    const item = itemFromId(req.params.albumid);
    if (!item) {
      throw new Error("No such album");
    }
    const out = await getBook(item, "cbz", req.personaid, req.options);
    res.download(out.file, out.title);
  });
  app.getAsync("/epub/:albumid", contentkeyauth, async (req, res) => {
    const item = itemFromId(req.params.albumid);
    if (!item) {
      throw new Error("No such album");
    }
    const out = await getBook(item, "epub", req.personaid, req.options);
    res.download(out.file, out.title);
  });

  app.getAsync("/jobs", contentkeyauth, async (req, res) => {
    await app.result(req, res, getJobs(req.options));
  });

  app.postAsync("/job", contentkeyauth, async (req, res) => {
    await app.result(
      req,
      res,
      createJob(req.body.albumid, req.body.format, req.personaid, req.options),
    );
  });

  app.getAsync("/job/:jobid", contentkeyauth, async (req, res) => {
    await app.result(req, res, getJob(req.params.jobid, req.options.lang));
  });

  app.delete("/job/:joibid", contentkeyauth, async (req, res) => {
    await app.result(req, res, deleteJob(req.params.jobid));
  });

  app.getAsync("/job/:jobid/stream", contentkeyauth, async (req, res) => {
    await streamJob(req.params.jobid, req.options, res);
  });

  app.getAsync("/recent", contentkeyauth, async (req, res) => {
    await app.result(req, res, recent(req.personaid));
  });

  app.getAsync("/rescanrecent", async (req, res) => {
    await app.result(req, res, rescanFiles());
  });

  app.getAsync("/folder/:folderid", contentkeyauth, async (req, res) => {
    // file deepcode ignore PT: <please specify a reason of ignoring this>
    await app.result(req, res, folder(req.params.folderid, req.personaid, req.options));
  });

  app.getAsync("/search/:q", contentkeyauth, async (req, res) => {
    await app.result(req, res, search(req.params.q, req.personaid, req.options));
  });

  app.getAsync("/folder", contentkeyauth, async (req, res) => {
    await app.result(req, res, folder(undefined, req.personaid, req.options));
  });
  app.getAsync("/clean", async (req, res) => {
    app.result(req, res, null);
  });

  // Background rescan
  (async () => {
    while (true) {
      await rescanFiles();
      if (config.clientMode === "photo") {
        await scanPictureData(".");
      }
      await sleep(config.scaninterval);
    }
  })();
}

export async function close() {
  await bookDB.close();
  await fsCache.close();
}
