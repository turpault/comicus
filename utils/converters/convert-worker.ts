/*
    convert-worker.ts
    Copyright (c) Jocelyn Turpault 2021
*/

import d from "debug";
import { expose } from "../../utils/worker";
import { streamToFile } from "../fsutils";
import { DecompressedData, Item } from "../types";
import { convert as cbzConvert } from "./cbz";
import { convert as epubConvert } from "./epub";
import { convert as pdfConvert } from "./pdf";

expose({
  convert: async (
    item: Item,
    format: "cbz" | "epub" | "pdf",
    files: DecompressedData[],
    target: string
  ): Promise<Error | undefined> => {
    const debug = d(`convert-worker: ${item.text}`);
    try {
      debug("Start");
      const converters = {
        cbz: cbzConvert,
        epub: epubConvert,
        pdf: pdfConvert,
      };

      const f = await converters[format](item, files);
      await streamToFile(f, target);

      debug("End");
    } catch (err) {
      return err;
    }
    return;
  },
});
